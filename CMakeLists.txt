#----------------------------------------------------------------------------
# Setup the project
set(THE_PROJECT GCD_v16)
set(THE_PROGRAM "Delacour")

cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(${THE_PROJECT})


#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
#option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" OFF)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()

  if(NOT CMAKE_BUILD_TYPE)
    # Default to a Release build if nothing else...
#    set(CMAKE_BUILD_TYPE Debug
    set(CMAKE_BUILD_TYPE Debug
      CACHE STRING "Choose the type of build, options are: None Release TestRelease MinSizeRel Debug RelWithDebInfo MinSizeRel Maintainer."
      FORCE
      )
  else()
    # Force to the cache, but use existing value.
    set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}"
      CACHE STRING "Choose the type of build, options are: None Release TestRelease MinSizeRel Debug RelWithDebInfo MinSizeRel Maintainer."
      FORCE
      )
  endif()

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
#
include(${Geant4_USE_FILE})

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
include_directories(${PROJECT_SOURCE_DIR}/include
                    ${Geant4_INCLUDE_DIR})
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(${THE_PROGRAM} ${THE_PROGRAM}.cc ${sources} ${headers})
target_link_libraries(${THE_PROGRAM} ${Geant4_LIBRARIES} )

#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build GCD_v11. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
#set(GCD_v11_SCRIPTS
#    McGonagall.in McGonagall.large_N.in McGonagall.out run1.mac run2.mac vis.mac
#  )

file(COPY in DESTINATION .)
file(COPY man DESTINATION .)

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/out)
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/log)

#----------------------------------------------------------------------------
# Add program to the project targets
# (this avoids the need of typing the program name after make)
#
add_custom_target(${THE_PROJECT} DEPENDS ${THE_PROGRAM})

#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
#install(TARGETS McGonagall DESTINATION bin)

