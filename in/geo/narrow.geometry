[constants]

AC:width  = 120 cm
det:width = 100 cm
calo:width = 80 cm


AC,ToF:strips = 12
AC,ToF:offset = 0 um
AC,ToF:p-bins = 20
AC,ToF:t-bins = 50
AC,ToF:t-min  = 0 ns
AC,ToF:t-max  = 20 ns
speed-of-light  = 0.5 c

AC:pSty-thickness = 2 cm
AC:Al-thickness = 30 mm
AC:Al-type = 48
AC:CFRP-thickness = 0.8 mm

ToF:pSty-thickness = 2 cm
ToF:Al-thickness = 30 mm
ToF:Al-type = 16
ToF:CFRP-thickness = 0.8 mm

S3,S4:pSty-thickness = 1.2 cm
S3,S4:Al-type      = 16
S3,S4:Al-thickness = 28.4 mm
S3,S4:CFRP-thickness = 0.8 mm

strip-thickness = 0.3 mm
strip-spreading = -1 um  ; gaussian sigma
C:strip-pitch   = 100 um
CC1:strip-pitch = 100 um

C:Al-thickness   = 28.4 mm
C:Al-type        = 16
C:CFRP-thickness = 0.8 mm


[world]

supports   = on
semiheight = 4 m
y-width    = 4 m
z-width    = 4 m


[gaps]
# and distances

AC--C      = 5 mm
C--S1      = 5 mm
S1--S2     = 50 cm ; not actual gap but a distance between centers of scintilators
S2--CC1    = 5 mm
S2--S3     = 40 cm ; once again, between centers
S3--CC2    = 5 mm
CC2--S4    = 5 mm
S4--ND     = 1 cm
ND--bottom-stuff = 1 cm

topAC--sideAC = -2 cm ; between bottom of topAC and top of sideAC scintilators
topAC<>sideAC = 0 ! ; between adjacent lateral sides of topAC and sideAC scintilators

S2--TD = 0 !
S2<>TD = 2 cm

# <fallback>
CC2--plate = 5 mm
plate--S4 = 5 mm
# </fallback>



#################
##  Detectors  ##
#################

[topAC]

supports     = on

width = $AC:width

n-strips       = $AC,ToF:strips
strip-offset   = $AC,ToF:offset
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light

pSty-thickness = $AC:pSty-thickness
pSty-y-width   = $topAC.width
pSty-z-width   = $topAC.width

Al-thickness = $AC:Al-thickness
Al-y-width   = $topAC.width
Al-z-width   = $topAC.width
Al-type      = $AC:Al-type

CFRP-thickness = $AC:CFRP-thickness
CFRP-y-width   = $topAC.width
CFRP-z-width   = $topAC.width


[sideAC]

length    = 110 cm ; X-extension
thickness = 12 mm
strips-horizintal = no

n-strips       = $AC,ToF:strips
strip-offset   = $AC,ToF:offset
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light


[C]

supports  = on

width = $det:width

n-layers  = 13
spreading = $strip-spreading
gaps      = 5 mm  all         ; between assemblies

y-pitch   = $C:strip-pitch
z-pitch   = $C:strip-pitch
Si-thickness = $strip-thickness  all ; [n-layers]
Si-y-width   = $C.width
Si-z-width   = $C.width

W-thickness  = 0  0  0  0.1  0.1  0.1  0.1  0.1  0.1  0.1  0.1  0  0  Xo ; [n-layers]
W-y-width    = $C.width
W-z-width    = $C.width

Al-thickness = $C:Al-thickness  all ; [n-layers + 1]
Al-type      = $C:Al-type  all ; [n-layers + 1]
Al-y-width   = $C.width
Al-z-width   = $C.width

CFRP-thickness  = $C:CFRP-thickness  all ; [n-layers + 1]
CFRP-y-width    = $C.width
CFRP-z-width    = $C.width

interleave-layers = no


[S1]

supports = on

width = $det:width

n-strips       = $AC,ToF:strips
strip-offset   = $AC,ToF:offset
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness
pSty-y-width   = $S1.width
pSty-z-width   = $S1.width

Al-thickness = $ToF:Al-thickness
Al-y-width   = $S1.width
Al-z-width   = $S1.width
Al-type      = $ToF:Al-type

CFRP-thickness = $ToF:CFRP-thickness
CFRP-y-width   = $S1.width
CFRP-z-width   = $S1.width


[S2]

supports = on

width = $calo:width

n-strips       = $AC,ToF:strips
strip-offset   = $AC,ToF:offset
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light

pSty-thickness = $ToF:pSty-thickness
pSty-y-width   = $S2.width
pSty-z-width   = $S2.width

Al-thickness = $ToF:Al-thickness
Al-y-width   = $S2.width
Al-z-width   = $S2.width
Al-type      = $ToF:Al-type

CFRP-thickness = $ToF:CFRP-thickness
CFRP-y-width   = $S2.width
CFRP-z-width   = $S2.width


[CC1]

supports  = on

width = $calo:width

n-layers  = 2
spreading = $strip-spreading

gaps(CsI--Si) = 1 cm  all ; [n-layers]
gaps(Si--CsI) = 1 cm  all ; [n-layers - 1]

y-pitch   = $CC1:strip-pitch
z-pitch   = $CC1:strip-pitch
Si-thickness = $strip-thickness  all ; [n-layers]
Si-y-width   = $CC1.width
Si-z-width   = $CC1.width

CsI-thickness  = 2 cm  all ; [n-layers]
CsI-y-width    = $CC1.width
CsI-z-width    = $CC1.width

Al(CsI)-thickness = 27 mm  all ; [n-layers]
Al(CsI)-type      = 48  all ; [n-layers]
Al(CsI)-y-width   = $CC1.width
Al(CsI)-z-width   = $CC1.width
Al(Si)-thickness  = 28.4 mm  all ; [n-layers]
Al(Si)-type       = 16  all ; [n-layers]
Al(Si)-y-width    = $CC1.width
Al(Si)-z-width    = $CC1.width

CFRP(CsI)-thickness  = 1.5 mm  all ; [n-layers + 1]
CFRP(CsI)-y-width    = $CC1.width
CFRP(CsI)-z-width    = $CC1.width
CFRP(Si)-thickness  = 1.5 mm  all ; [n-layers + 1]
CFRP(Si)-y-width    = $CC1.width
CFRP(Si)-z-width    = $CC1.width

# <fallback>
Al-thickness = 27  27  28.4 mm ; [n-layers + 1]
Al-type      = 16  all ; [n-layers + 1]
Al-y-width   = $CC1.width
Al-z-width   = $CC1.width
CFRP-thickness  = 1.5  1.5  0.8 mm ; [n-layers + 1]
CFRP-y-width    = $CC1.width
CFRP-z-width    = $CC1.width
gaps      = 1 cm  all         ; between assemblies
# </fallback>


[S3]

supports = on

width = $calo:width

n-strips       = 1
strip-offset   = 0 um
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light

pSty-thickness = $S3,S4:pSty-thickness
pSty-y-width   = $S3.width
pSty-z-width   = $S3.width

Al-thickness = $S3,S4:Al-thickness
Al-y-width   = $S3.width
Al-z-width   = $S3.width
Al-type      = $S3,S4:Al-type

CFRP-thickness = $S3,S4:CFRP-thickness
CFRP-y-width   = $S3.width
CFRP-z-width   = $S3.width


[CC2]

supports  = on
segmented = yes

n-layers = 10
CFRP-between-thickness = 3 mm ; support between layers
mirrorfilm-thickness   = 0.1 um ; currently, just vacuum
item-heights          = 36 mm  all ; [n-layers]  heights of scintilator cuboids

n-items[Y]  = 21 22  21 22  21 22  21 22  21 22 ; [n-layers]
n-items[Z]  = 22 21  22 21  22 21  22 21  22 21 ; [n-layers]
scint-width = 36 mm  all ; [n-layers]  currently, y = z
CFRP-between-items-thickness[Y] = 0.5 0  0.5 0  0.5 0  0.5 0  0.5 0 mm ; [n-layers]
CFRP-between-items-thickness[Z] = 0 0.5  0 0.5  0 0.5  0 0.5  0 0.5 mm ; [n-layers]


[plate]

upper-outer-thickness = 1 cm
upper-inner-thickness = 7 cm
lower-inner-thickness = 15 cm
lower-outer-thickness = 1.5 cm
y-width   = $calo:width
z-width   = $calo:width

# <fallback>
thickness = 2 cm
# </fallback>


[S4]

supports = on

width = $calo:width

n-strips       = 1
strip-offset   = 0 um
p-bins         = $AC,ToF:p-bins
t-bins         = $AC,ToF:t-bins
t-min          = $AC,ToF:t-min
t-max          = $AC,ToF:t-max
speed-of-light = $speed-of-light

pSty-thickness = $S3,S4:pSty-thickness
pSty-y-width   = $S4.width
pSty-z-width   = $S4.width

Al-thickness = $S3,S4:Al-thickness
Al-y-width   = $S4.width
Al-z-width   = $S4.width
Al-type      = $S3,S4:Al-type

CFRP-thickness = $S3,S4:CFRP-thickness
CFRP-y-width   = $S4.width
CFRP-z-width   = $S4.width


[ND]

supports = off

width = $calo:width

pEth1-thickness = 0.001 um
PMMA1-thickness = 0.001 um
LiFZnS1-thickness = 0.001 um
PMMA2-thickness = 0.001 um
pEth2-thickness = 0.001 um
PMMA3-thickness = 0.001 um
LiFZnS2-thickness = 0.001 um
PMMA4-thickness = 0.001 um
pEth3-thickness = 0.001 um

y-width   = $ND.width
z-width   = $ND.width

Al-thickness = 0.001 um
Al-y-width   = $ND.width
Al-z-width   = $ND.width
Al-type      = $S3,S4:Al-type

CFRP-thickness = 0.001 um
CFRP-y-width   = $ND.width
CFRP-z-width   = $ND.width



[Bottom Stuff]

supports = on

x-size = 0 m
y-size = 1.5 m
z-size = 1.5 m

[TD]

length = 70 cm
thickness = 1 cm
