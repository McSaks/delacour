/*        //                                          \\ 
//       /  \              3D sensitive              /  \ 
//      /o  o\          calorimeter system          /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef Calo3DSystem_h
#define Calo3DSystem_h 1

#include "globals.hh"
#include <vector>
#include "types.hh"

class DetectorConstruction;
class DetectorMessenger;
class EventAction;
class EventData;
class Edep3D;


class Calo3DSystem
{
  public:
       
       // constructors / destructors :
       Calo3DSystem();
      ~Calo3DSystem();
       /* _PLACEHOLDER_ */
       // set / get :
       inline Edep3D* GetArray() const { return array; };
       void SetDetectorConstruction(DetectorConstruction* det) { detector = det; };
       void SetMessenger(DetectorMessenger* mess) { messenger = mess; };
       void SetEventAction(EventAction* ev) { currentEvent = ev; };
       void SetEventData(EventData* ev) { data = ev; };
       
       // others :
       void Clear();
       G4bool Trigger() const;
       G4bool Write() const;
   private:
//       G4bool Trigger1() const;
       
  public:
       Edep3D* array;
       G4double* etot;
  protected:
       DetectorConstruction* detector;
       DetectorMessenger* messenger;
       EventAction* currentEvent;
       EventData* data;
};

#endif
