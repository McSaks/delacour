/*        //                                          \\ 
//       /  \              Calorimetric              /  \ 
//      /o  o\          (Energy-sensitive)          /o  o\ 
//     /  ..  \               System               /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef EnergySystem_h
#define EnergySystem_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>
#include "types.hh"

class DetectorConstruction;
class DetectorMessenger;
class EventAction;
class PositionSystem;
class TrackPoint;
class EventData;


class EnergyPoint
{
  public:
       EnergyPoint()
           : edep(0), amplitude(0), enabled(false)
	   , SDtype(kOtherDetector), detPart(0), detLayer(-1) { };
       void On()     { enabled = true; };
       void Off()    { enabled = false; };
       void Switch() { enabled -= 1; };
       EnergyPoint(const TrackPoint&);
              
  public:
       G4double edep;
       G4double amplitude;
       G4bool enabled;
       G4String SDname;
       EDetectorType SDtype;
       G4int detPart; // 1 <- C1, 2 <- C2
       G4int detLayer;
};

typedef std::vector<EnergyPoint> EnergyPoints;



class EnergySystem
{
  public:
       
       // constructors / destructors :
       EnergySystem();
       //PositionSystem(const TrackPoints& pts, const TrackLine& l = TrackLine())
       //    : trackPoints(pts), trackLine(l) { };
       //PositionSystem(const TrackLine& l)
       //    : trackPoints(), trackLine(l) { };
      ~EnergySystem();
       
       // set / get :
       void SetDetectorConstruction(DetectorConstruction* det) { detector = det; };
       void SetMessenger(DetectorMessenger* mess) { messenger = mess; };
       void SetEventAction(EventAction* ev) { currentEvent = ev; };
       void SetEventData(EventData* ev) { data = ev; };
       void SetPositionSystem(PositionSystem* p) { posSystem = p; };

       // others :
       void Append(const EnergyPoint&);
       EnergySystem& operator+=(const EnergyPoint& pt)
           { Append(pt); return *this; };
       void Clear() { };
       G4bool Write() const;
       
  private:
//       G4bool Trigger1() const;
       
  protected:
       EnergyPoints points;
       DetectorConstruction* detector;
       DetectorMessenger* messenger;
       EventAction* currentEvent;
       EventData* data;
       PositionSystem* posSystem;
};

#endif
