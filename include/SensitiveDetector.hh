#ifndef SensitiveDetector_h
#define SensitiveDetector_h

#include "G4VSensitiveDetector.hh"
#include "types.hh"

class DetectorConstruction;

class SensitiveDetector : public G4VSensitiveDetector
{
  public:
    SensitiveDetector(G4String name)
      : G4VSensitiveDetector(name), SDtype(kOtherDetector)
      , detConstr(NULL), fPV(NULL)
      , fCopyNo(-1), detPart(-1) { };
    EDetectorType GetDetectorType() { return SDtype; };
    G4int GetCopyNo() { return fCopyNo; };
    
  protected:
    EDetectorType SDtype;
    DetectorConstruction* detConstr;
    G4VPhysicalVolume* fPV;
    G4int fCopyNo; // G4int& detLayer = fCopyNo;
    G4int detPart;
    G4ThreeVector fpos, fdims;
};

#endif
