/*        //                                          \\ 
//       /  \          Formatting Functions          /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \         some handy string          /  ..  \ 
//    /__\__/__\      manipulating functions      /__\__/__\ 
*/

#ifndef TFORMAT_h
#define TFORMAT_h 1

#include "globals.hh"
				namespace TFormat    {

G4String blanks(G4int n, G4String c = " ");
G4String blanks(G4int n, char c);

G4String digitblock(const G4int num);

G4String postfix(const G4int num);

std::ostream& progress(std::ostream& out = G4cout, G4int iev = 0, G4int nev = 0,
                  const char* name = "event", G4int minprint = 1000);

G4String cropblanksleft(G4String str);

G4String cropblanksright(G4String str);

G4String digitblockcrop(const G4int num);

G4String itoa(G4int num, G4int buffersize = 32);

G4String ftoa(G4double num, G4int buffersize = 32);

G4String centered(G4String, const str_size, const char = ' ');

G4String padright(G4String, const str_size, const char = ' ');

G4String padleft(G4String, const str_size, const char = ' ');

#include <stdlib.h>

				/* namespace TFormat */ }

#endif
