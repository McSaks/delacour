/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\     Sensitive Strip-Type Detector    /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef SensitiveScintStrip_h
#define SensitiveScintStrip_h 1

#include "SensitiveStrip.hh"
#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include "HitScintStrip.hh"
#include "DetectorConstruction.hh"
#include "ScintillationSystem.hh"
#include "geomdefs.hh"
#include <vector>
#include "types.hh"

class G4Step;
class G4HCofThisEvent;
class DetectorConstruction;

class SensitiveScintStrip : public SensitiveStrip
{
  public:
    void Add(ScintillationSystem* sys) { sys->layers.push_back(&data); };
    SensitiveScintStrip(
        G4String name,
        DetectorConstruction*,
        G4VPhysicalVolume*,
        ScintillationSystem*,
        G4int strips,
        G4int det_part,
        G4int copyNo,
        EDetectorType = kOtherDetector,
        EStripProj ss = kNoStripProj,
        EStripProj pp = kNoStripProj);
   ~SensitiveScintStrip();
    
    /*void Setup(
      G4double t_min,
      G4double t_max,
      G4double t_bin,
      G4double p_bin,
      G4double speedoflight);*/
    void Setup(
      G4double t_min,
      G4double t_max,
      G4int    t_nbins,
      G4int    p_nbins,
      G4int    s_nbins,
      G4double speedoflight);
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);
    void SetSOffset(const G4double& o);
    inline G4String GetStripAxisName() const { return _EStripProjSimple[sAxis]; };
    inline G4String GetAlongAxisName() const { return _EStripProjSimple[pAxis]; };
    inline G4bool HasSignal() const { return hassignal; };
    inline G4double GetEarliest() const { return earliest; };
    inline G4double GetLatest() const { return latest; };
    inline EStripProj GetSAxis() const { return sAxis; }; // across strips
    inline EStripProj GetPAxis() const { return pAxis; }; // along strips
    inline EStripProj GetNAxis() const { return EStripProj(kAllStripProj &~ sAxis &~ pAxis); }; // normal to plane
       
  public:
    G4double earliest, latest;
    G4double Etot;
    std::vector<G4double>* stripEtot;
    G4bool hassignal;
  
  public:
    ScintillationSystem* system;
    HitsCollectionScintStrip* hitsCollection;
    ScintStripLayer data;
    static const G4double kNoSignal;
    
  private:
    inline G4double x_minmax(short sign, G4Box* box) {
      if (!box) box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
      return (sign > 0) ?
        fPV->GetTranslation().x() + box->GetXHalfLength():
        fPV->GetTranslation().x() - box->GetXHalfLength(); }
    inline G4double y_minmax(short sign, G4Box* box) {
      if (!box) box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
      return (sign > 0) ?
        fPV->GetTranslation().y() + box->GetYHalfLength():
        fPV->GetTranslation().y() - box->GetYHalfLength(); }
    inline G4double z_minmax(short sign, G4Box* box) {
      if (!box) box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
      return (sign > 0) ?
        fPV->GetTranslation().z() + box->GetZHalfLength():
        fPV->GetTranslation().z() - box->GetZHalfLength(); }
       
  protected:
    EStripProj sAxis, pAxis;
};

#endif
