/*        //                                          \\ 
//       /  \           Process  Parameters          /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef ProcessParameters_h
#define ProcessParameters_h 1


G4String outfile = "out/default.eventdata";
G4String rundatafile = "out/default.rundata";
G4String macrofile = "";
G4String geofile = "in/geo/default.geometry";
std::vector<G4String> commands;
G4bool ui_use = false;
//G4bool use_trigger = true;
E_use_trigger use_trigger = kTrigger_all;
G4bool ToFTimeSelection = true;
EScintStripOutput scintstripoutput = kScintStrip_tree;
G4long nevents = 0; G4String nevents_str = "0";
enum EPhysicsList {kPhysicsList_simple, kPhysicsList_QGSP_BERT_HP, kPhysicsList_QGSP_BIC_HP}
  physics_list = kPhysicsList_QGSP_BERT_HP;
G4bool ini_allow_default = false;
long seed = -1;


G4bool OptErr(const char* name, const char* opt, const char* arg)
{
  G4cerr << "Argument of \u2018" << opt << "\u2019 option must be one of the following:" << G4endl;
  IF (false)
  ELSE_IF (!strcmp(name, "trigger"))
    G4cerr << "     \u2018off\u2019  or  \u2018none\u2019," << G4endl;
    G4cerr << "     \u2018pos\u2019  or  \u2018trk\u2019," << G4endl;
    G4cerr << "     \u2018tof\u2019  or  \u2018ToF\u2019," << G4endl;
    G4cerr << "     \u2018all\u2019." << G4endl;
  ELSE_IF (!strcmp(name,"ScintStripOutput"))
    G4cerr << "     \u2018array\u2019," << G4endl;
    G4cerr << "     \u2018tree\u2019   or" << G4endl;
    G4cerr << "     \u2018signal\u2019." << G4endl;
  ELSE_IF (!strcmp(name,"PhysicsList"))
    G4cerr << "     \u2018simple\u2019,  \u2018glast\u2019  or  \u2018QGSP_BIC_HP\u2019." << G4endl;
  ELSE
    G4cerr << "     ( candidates not found )." << G4endl;
  END_IF
  G4cerr << "   Recieved: \u2018" << arg << "\u2019.\n" << G4endl;
  return 0;
}


G4bool ArgErr(const char* opt, const char* name, G4int n = 1)
{
  G4cerr << "Error while parsing options:\n   ";
  WHICH
    INCASE  !strcmp(name, "unknown")  THEN
      G4cerr << "Unknown option \u2018" << opt << "\u2019.\n" << G4endl;
      return 0;
    INCASE  !strcmp(name, "{")  THEN
      G4cerr << "Command block began by \u2018{\u2019 must be closed by \u2018}\u2019.\n" << G4endl;
      return 0;
    INCASE  !strcmp(name, "trigger")  THEN
      return OptErr("trigger", "--trigger", opt);
    INCASE  !strcmp(name, "ScintStripOutput")  THEN
      return OptErr("ScintStripOutput", "--ScintStripOutput", opt);
    INCASE  !strcmp(name, "PhysicsList")  THEN
      return OptErr("PhysicsList", "--physicsList", opt);
  END_WHICH
  G4cerr << "Option \u2018" << opt << "\u2019 ";
  if(strcmp(opt, name)) G4cerr << "(\u2018" << name << "\u2019) ";
  G4cerr << "must have " << n << " argument" << (n == 1 ? "" : "s") << ".\n" << G4endl;
  return 0;
}


G4bool ProcessParameters(int argc, char* argv[])
{
  char* opt;
  //int optNo = 0;
  FORs(ix, 1, argc) DO
    opt = argv[ix];
    WHICH
      
      // shortcuts
      INCASE  !strcmp(opt, "--none")  THEN
          macrofile = "in/mac/batch0.mac";
          outfile = "/dev/null";
          rundatafile = "/dev/null";
          nevents = 0; nevents_str = "0";
      
      INCASE  !strcmp(opt, "--noRun")  ||
              !strcmp(opt,  "-N")     THEN
          macrofile = "in/mac/batch0.mac";
          nevents = 0; nevents_str = "0";
      
      INCASE  !strcmp(opt, "--vis")  THEN
          macrofile = "in/mac/mainvis.mac";
          //outfile = "/dev/null";
          ui_use = true;
      
      // 0-param options
      INCASE  !strcmp(opt, "--help")  ||
              !strcmp(opt,  "-h")    THEN
          IF(ix>=argc-1) usage();
          ELSE
            G4String doc = argv[++ix], docfile = "man/usage.txt";
            WHICH
              INCASE doc == "eventdata"     THEN docfile = "man/eventdata.format";
              INCASE doc == "eventdata.CC2" THEN docfile = "man/eventdata.CC2.format";
              INCASE doc == "cc2"           THEN docfile = "man/eventdata.CC2.format";
              INCASE doc == "CC2"           THEN docfile = "man/eventdata.CC2.format";
              INCASE doc == "rundata"       THEN docfile = "man/rundata.format";
              INCASE doc == "geometry"      THEN docfile = "man/geometry.format";
              INCASE doc == "geo"           THEN docfile = "man/geometry.format";
              INCASE doc == "whatsnew"      THEN docfile = "man/whatsnew.txt";
              INCASE doc == "new"           THEN docfile = "man/whatsnew.txt";
            END_WHICH
            usage(docfile);
          END_IF
          return 0;
      
      INCASE !strcmp(opt, "--version") THEN
          G4cout << version_string << G4endl;
          return 0;

      INCASE  !strcmp(opt, "--ui")           ||
              !strcmp(opt, "--gui")          ||
              !strcmp(opt, "--interactive")  ||
              !strcmp(opt,  "-i")           THEN
          ui_use = true;
      
      INCASE  !strcmp(opt, "--no-ui")   ||
              !strcmp(opt, "--no-gui")  ||
              !strcmp(opt, "--batch")   ||
              !strcmp(opt,  "-b")      THEN
          ui_use = false;
      
      // 1-param options
      
      INCASE  !strcmp(opt, "--out")        ||
              !strcmp(opt, "--data")       ||
              !strcmp(opt, "--eventdata")  ||
              !strcmp(opt,  "-o")         THEN
          if(ix>=argc-1) return ArgErr(opt, "--eventdata");
          outfile = GetFileName("out", "eventdata", argv[++ix]);
      
      INCASE  !strcmp(opt, "--detector")           ||
              !strcmp(opt, "--detector-geometry")  ||
              !strcmp(opt, "--geometry")           ||
              !strcmp(opt,  "-g")                 THEN
          if(ix>=argc-1) return ArgErr(opt, "--geometry");
          geofile = GetFileName("in/geo", "geometry", argv[++ix]);
      
      INCASE  !strcmp(opt, "--macro")           ||
              !strcmp(opt, "/control/execute")  ||
              !strcmp(opt,  "-m")              THEN
          if(ix>=argc-1) return ArgErr(opt, "--macro");
          macrofile = GetFileName("in/mac", "mac", argv[++ix]);
      
      INCASE  !strcmp(opt, "--nevents")    ||
              !strcmp(opt, "--nEvents")    ||
              !strcmp(opt, "/run/beamOn")  ||
              !strcmp(opt,  "-n")         THEN
          if(ix>=argc-1) return ArgErr(opt, "--nevents");
          nevents_str = argv[++ix];
          nevents = atol(nevents_str);
      
      INCASE  !strcmp(opt, "--trigger")  ||
              !strcmp(opt,  "-t")       THEN
          if(ix>=argc-1) return ArgErr(opt, "--trigger");
          opt = argv[++ix];
          WHICH
            INCASE !strcmp(opt, "off")  || !strcmp(opt, "none") THEN use_trigger = kTrigger_none;
            INCASE !strcmp(opt, "pos")  || !strcmp(opt, "trk")  THEN use_trigger = kTrigger_pos;
            INCASE !strcmp(opt, "tof")  || !strcmp(opt, "ToF")  THEN use_trigger = kTrigger_tof, ToFTimeSelection = true;
            INCASE !strcmp(opt, "tof-") || !strcmp(opt, "ToF-") THEN use_trigger = kTrigger_tof, ToFTimeSelection = false;
            INCASE !strcmp(opt, "all")                          THEN use_trigger = kTrigger_all;
            ELSE return ArgErr(opt, "trigger");
          END_WHICH
      
      INCASE  !strcmp(opt, "--ScintStripOutput")  ||
              !strcmp(opt,  "-s")                THEN
          if(ix>=argc-1) return ArgErr(opt, "--ScintStripOutput");
          opt = argv[++ix];
          WHICH
            INCASE !strcmp(opt, "array")  THEN scintstripoutput = kScintStrip_array;
            INCASE !strcmp(opt, "tree")   THEN scintstripoutput = kScintStrip_tree;
            INCASE !strcmp(opt, "signal") THEN scintstripoutput = kScintStrip_PMSignals;
            ELSE return ArgErr(opt, "ScintStripOutput");
          END_WHICH
      
      INCASE  !strcmp(opt, "--PhysicsList")  ||
              !strcmp(opt, "--physicsList")  ||
              !strcmp(opt, "--physics")      ||
              !strcmp(opt,  "-ph")          THEN
          if(ix>=argc-1) return ArgErr(opt, "--physicsList");
          opt = argv[++ix];
          WHICH
            INCASE !strcmp(opt, "simple")      THEN physics_list = kPhysicsList_simple;
            INCASE !strcmp(opt, "QGSP_BERT_HP")       THEN physics_list = kPhysicsList_QGSP_BERT_HP;
            INCASE !strcmp(opt, "QGSP_BIC_HP") THEN physics_list = kPhysicsList_QGSP_BIC_HP;
            ELSE return ArgErr(opt, "PhysicsList");
          END_WHICH
      
      INCASE  !strcmp(opt, "--rundata")  ||
              !strcmp(opt, "--runData")  ||
              !strcmp(opt,  "-r")       THEN
          if(ix>=argc-1) return ArgErr(opt, "--rundata");
          rundatafile = GetFileName("out", "rundata", argv[++ix]);
      
      INCASE  !strcmp(opt, "--compatibility")  ||
              !strcmp(opt, "--allow-default") THEN
          ini_allow_default = true;
      
      INCASE  !strcmp(opt, "--random-seed")    ||
              !strcmp(opt,  "-S")         THEN
          if(ix>=argc-1) return ArgErr(opt, "--random-seed");
          seed = atol(argv[++ix]);
      
      INCASE  !strcmp(opt, "{") ||
              !strcmp(opt, "/:")  THEN                // begin of command block
          if(ix>=argc-1) return ArgErr(opt, "{");     //   check if `{` is not last argument
          commands.push_back(G4String());             //   create base for first command in this block
          WHILE( strcmp(opt = argv[++ix], "}") &&     //   while not end of command block:
                 strcmp(opt, ":/") )                  //
              if(ix>=argc-1) return ArgErr(opt, "{"); //     check if arguments don't end inside block
              IF_ strcmp(opt, ",") THEN               //     non-comma continues command:
                  commands.back() += opt;             //       append next word ...
                  commands.back() += " ";             //       ... and whitespace
              ELSE // ",", end of command             //     comma ends command:
                  commands.push_back(G4String());     //       create base for next command in the block
              END_IF                                  //     end if
          END_WHILE                                   //   end while
          //opt = argv[++ix];                         // end of command block
          
      // numbered options
      ELSE  return ArgErr(opt, "unknown");
  
    END_WHICH
  
  END_FOR
  
  if ( macrofile.empty() ) {
    if(ui_use)
      macrofile = "in/mac/main.mac";
    else
      macrofile = "in/mac/batch.mac";
  }
  
  {std::ofstream f(outfile);
  if(!f) {G4cerr << "Error: data file " << outfile << " cannot be created." << G4endl; return false;}
  f << "# Created by " << version_string << G4endl;
  f.close();}
  {std::ofstream f(rundatafile, std::ofstream::out|std::ofstream::app);
  if(!f) {G4cerr << "Error: trigger file " << rundatafile << " cannot be created." << G4endl; return false;}
  // f << "# Created by " << version_string << G4endl;
  f.close();}
  {std::ifstream f(macrofile);
  if(!f) {G4cerr << "Error: macro file " << macrofile << " cannot be found." << G4endl; return false;}
  f.close();}
  {std::ifstream f(geofile);
  if(!f) {G4cerr << "Error: geometry file " << geofile << " cannot be found." << G4endl; return false;}
  f.close();}
  return true;
}


void ExecuteCommands(G4UImanager* UImanager) {
      if(commands.size()) {
        G4cout << "\n+------------------------------------------+\n| To be executed via command-line options: |" << G4endl;
        G4cout << "+------------------------------------------+\n|\n|" << G4endl;
        vFORc(G4String, commands, cmd) {
          G4cout << "|  Executing:  " << *cmd << G4endl;
          UImanager->ApplyCommand(*cmd);
        }
        G4cout << "|\n|\n+------------------------------------------+\n" << G4endl;
      }
}

#endif
