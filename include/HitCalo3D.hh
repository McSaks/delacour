/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\      Hit in the Calo-3D Detector     /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef HitCalo3D_h
#define HitCalo3D_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "types.hh"

class HitCalo3D : public G4VHit
{
  public:

      HitCalo3D();
     ~HitCalo3D();
      HitCalo3D(const HitCalo3D&);
      const HitCalo3D& operator=(const HitCalo3D&);
      G4int operator==(const HitCalo3D&) const;

      inline void* operator new(size_t);
      inline void  operator delete(void*);

      void Draw();
      void Print();

  public:
  
      void SetTrackID    (G4int track)       { trackID = track; };
      void SetEdep       (G4double de)       { edep = de; };
      void SetPos        (G4ThreeVector xyz) { pos = xyz; };
      
      G4int GetTrackID()      { return trackID; };
      G4double GetEdep()      { return edep; };      
      G4ThreeVector GetPos()  { return pos; };
      
  private:
  
      G4int         trackID;
      G4double      edep;
      G4ThreeVector pos;
};

typedef G4THitsCollection<HitCalo3D> HitsCollectionCalo3D;

extern G4Allocator<HitCalo3D> hitAllocatorCalo3D;

inline void* HitCalo3D::operator new(size_t)
{
  void* aHit;
  aHit = (void*) hitAllocatorCalo3D.MallocSingle();
  return aHit;
}

inline void HitCalo3D::operator delete(void* aHit)
{
  hitAllocatorCalo3D.FreeSingle((HitCalo3D*) aHit);
}

#endif
