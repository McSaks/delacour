# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake28

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /work/gamma400/Delacour

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /work/gamma400/Delacour

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake28 -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /work/gamma400/Delacour/CMakeFiles /work/gamma400/Delacour/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /work/gamma400/Delacour/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named Delacour

# Build rule for target.
Delacour: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 Delacour
.PHONY : Delacour

# fast build rule for target.
Delacour/fast:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/build
.PHONY : Delacour/fast

#=============================================================================
# Target rules for targets named GCD_v16

# Build rule for target.
GCD_v16: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 GCD_v16
.PHONY : GCD_v16

# fast build rule for target.
GCD_v16/fast:
	$(MAKE) -f CMakeFiles/GCD_v16.dir/build.make CMakeFiles/GCD_v16.dir/build
.PHONY : GCD_v16/fast

Delacour.o: Delacour.cc.o
.PHONY : Delacour.o

# target to build an object file
Delacour.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/Delacour.cc.o
.PHONY : Delacour.cc.o

Delacour.i: Delacour.cc.i
.PHONY : Delacour.i

# target to preprocess a source file
Delacour.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/Delacour.cc.i
.PHONY : Delacour.cc.i

Delacour.s: Delacour.cc.s
.PHONY : Delacour.s

# target to generate assembly for a file
Delacour.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/Delacour.cc.s
.PHONY : Delacour.cc.s

src/BoxUnion.o: src/BoxUnion.cc.o
.PHONY : src/BoxUnion.o

# target to build an object file
src/BoxUnion.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxUnion.cc.o
.PHONY : src/BoxUnion.cc.o

src/BoxUnion.i: src/BoxUnion.cc.i
.PHONY : src/BoxUnion.i

# target to preprocess a source file
src/BoxUnion.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxUnion.cc.i
.PHONY : src/BoxUnion.cc.i

src/BoxUnion.s: src/BoxUnion.cc.s
.PHONY : src/BoxUnion.s

# target to generate assembly for a file
src/BoxUnion.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxUnion.cc.s
.PHONY : src/BoxUnion.cc.s

src/BoxVolume.o: src/BoxVolume.cc.o
.PHONY : src/BoxVolume.o

# target to build an object file
src/BoxVolume.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxVolume.cc.o
.PHONY : src/BoxVolume.cc.o

src/BoxVolume.i: src/BoxVolume.cc.i
.PHONY : src/BoxVolume.i

# target to preprocess a source file
src/BoxVolume.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxVolume.cc.i
.PHONY : src/BoxVolume.cc.i

src/BoxVolume.s: src/BoxVolume.cc.s
.PHONY : src/BoxVolume.s

# target to generate assembly for a file
src/BoxVolume.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/BoxVolume.cc.s
.PHONY : src/BoxVolume.cc.s

src/Calo3DSystem.o: src/Calo3DSystem.cc.o
.PHONY : src/Calo3DSystem.o

# target to build an object file
src/Calo3DSystem.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Calo3DSystem.cc.o
.PHONY : src/Calo3DSystem.cc.o

src/Calo3DSystem.i: src/Calo3DSystem.cc.i
.PHONY : src/Calo3DSystem.i

# target to preprocess a source file
src/Calo3DSystem.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Calo3DSystem.cc.i
.PHONY : src/Calo3DSystem.cc.i

src/Calo3DSystem.s: src/Calo3DSystem.cc.s
.PHONY : src/Calo3DSystem.s

# target to generate assembly for a file
src/Calo3DSystem.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Calo3DSystem.cc.s
.PHONY : src/Calo3DSystem.cc.s

src/DetectorConstruction.o: src/DetectorConstruction.cc.o
.PHONY : src/DetectorConstruction.o

# target to build an object file
src/DetectorConstruction.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorConstruction.cc.o
.PHONY : src/DetectorConstruction.cc.o

src/DetectorConstruction.i: src/DetectorConstruction.cc.i
.PHONY : src/DetectorConstruction.i

# target to preprocess a source file
src/DetectorConstruction.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorConstruction.cc.i
.PHONY : src/DetectorConstruction.cc.i

src/DetectorConstruction.s: src/DetectorConstruction.cc.s
.PHONY : src/DetectorConstruction.s

# target to generate assembly for a file
src/DetectorConstruction.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorConstruction.cc.s
.PHONY : src/DetectorConstruction.cc.s

src/DetectorMessenger.o: src/DetectorMessenger.cc.o
.PHONY : src/DetectorMessenger.o

# target to build an object file
src/DetectorMessenger.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorMessenger.cc.o
.PHONY : src/DetectorMessenger.cc.o

src/DetectorMessenger.i: src/DetectorMessenger.cc.i
.PHONY : src/DetectorMessenger.i

# target to preprocess a source file
src/DetectorMessenger.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorMessenger.cc.i
.PHONY : src/DetectorMessenger.cc.i

src/DetectorMessenger.s: src/DetectorMessenger.cc.s
.PHONY : src/DetectorMessenger.s

# target to generate assembly for a file
src/DetectorMessenger.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/DetectorMessenger.cc.s
.PHONY : src/DetectorMessenger.cc.s

src/Directions.o: src/Directions.cc.o
.PHONY : src/Directions.o

# target to build an object file
src/Directions.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Directions.cc.o
.PHONY : src/Directions.cc.o

src/Directions.i: src/Directions.cc.i
.PHONY : src/Directions.i

# target to preprocess a source file
src/Directions.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Directions.cc.i
.PHONY : src/Directions.cc.i

src/Directions.s: src/Directions.cc.s
.PHONY : src/Directions.s

# target to generate assembly for a file
src/Directions.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Directions.cc.s
.PHONY : src/Directions.cc.s

src/EnergySystem.o: src/EnergySystem.cc.o
.PHONY : src/EnergySystem.o

# target to build an object file
src/EnergySystem.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EnergySystem.cc.o
.PHONY : src/EnergySystem.cc.o

src/EnergySystem.i: src/EnergySystem.cc.i
.PHONY : src/EnergySystem.i

# target to preprocess a source file
src/EnergySystem.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EnergySystem.cc.i
.PHONY : src/EnergySystem.cc.i

src/EnergySystem.s: src/EnergySystem.cc.s
.PHONY : src/EnergySystem.s

# target to generate assembly for a file
src/EnergySystem.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EnergySystem.cc.s
.PHONY : src/EnergySystem.cc.s

src/EventAction.o: src/EventAction.cc.o
.PHONY : src/EventAction.o

# target to build an object file
src/EventAction.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventAction.cc.o
.PHONY : src/EventAction.cc.o

src/EventAction.i: src/EventAction.cc.i
.PHONY : src/EventAction.i

# target to preprocess a source file
src/EventAction.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventAction.cc.i
.PHONY : src/EventAction.cc.i

src/EventAction.s: src/EventAction.cc.s
.PHONY : src/EventAction.s

# target to generate assembly for a file
src/EventAction.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventAction.cc.s
.PHONY : src/EventAction.cc.s

src/EventData.o: src/EventData.cc.o
.PHONY : src/EventData.o

# target to build an object file
src/EventData.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventData.cc.o
.PHONY : src/EventData.cc.o

src/EventData.i: src/EventData.cc.i
.PHONY : src/EventData.i

# target to preprocess a source file
src/EventData.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventData.cc.i
.PHONY : src/EventData.cc.i

src/EventData.s: src/EventData.cc.s
.PHONY : src/EventData.s

# target to generate assembly for a file
src/EventData.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/EventData.cc.s
.PHONY : src/EventData.cc.s

src/Geo3D.o: src/Geo3D.cc.o
.PHONY : src/Geo3D.o

# target to build an object file
src/Geo3D.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geo3D.cc.o
.PHONY : src/Geo3D.cc.o

src/Geo3D.i: src/Geo3D.cc.i
.PHONY : src/Geo3D.i

# target to preprocess a source file
src/Geo3D.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geo3D.cc.i
.PHONY : src/Geo3D.cc.i

src/Geo3D.s: src/Geo3D.cc.s
.PHONY : src/Geo3D.s

# target to generate assembly for a file
src/Geo3D.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geo3D.cc.s
.PHONY : src/Geo3D.cc.s

src/Geometry.o: src/Geometry.cc.o
.PHONY : src/Geometry.o

# target to build an object file
src/Geometry.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geometry.cc.o
.PHONY : src/Geometry.cc.o

src/Geometry.i: src/Geometry.cc.i
.PHONY : src/Geometry.i

# target to preprocess a source file
src/Geometry.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geometry.cc.i
.PHONY : src/Geometry.cc.i

src/Geometry.s: src/Geometry.cc.s
.PHONY : src/Geometry.s

# target to generate assembly for a file
src/Geometry.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Geometry.cc.s
.PHONY : src/Geometry.cc.s

src/HitCalo3D.o: src/HitCalo3D.cc.o
.PHONY : src/HitCalo3D.o

# target to build an object file
src/HitCalo3D.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitCalo3D.cc.o
.PHONY : src/HitCalo3D.cc.o

src/HitCalo3D.i: src/HitCalo3D.cc.i
.PHONY : src/HitCalo3D.i

# target to preprocess a source file
src/HitCalo3D.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitCalo3D.cc.i
.PHONY : src/HitCalo3D.cc.i

src/HitCalo3D.s: src/HitCalo3D.cc.s
.PHONY : src/HitCalo3D.s

# target to generate assembly for a file
src/HitCalo3D.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitCalo3D.cc.s
.PHONY : src/HitCalo3D.cc.s

src/HitEnergy.o: src/HitEnergy.cc.o
.PHONY : src/HitEnergy.o

# target to build an object file
src/HitEnergy.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitEnergy.cc.o
.PHONY : src/HitEnergy.cc.o

src/HitEnergy.i: src/HitEnergy.cc.i
.PHONY : src/HitEnergy.i

# target to preprocess a source file
src/HitEnergy.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitEnergy.cc.i
.PHONY : src/HitEnergy.cc.i

src/HitEnergy.s: src/HitEnergy.cc.s
.PHONY : src/HitEnergy.s

# target to generate assembly for a file
src/HitEnergy.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitEnergy.cc.s
.PHONY : src/HitEnergy.cc.s

src/HitScintStrip.o: src/HitScintStrip.cc.o
.PHONY : src/HitScintStrip.o

# target to build an object file
src/HitScintStrip.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitScintStrip.cc.o
.PHONY : src/HitScintStrip.cc.o

src/HitScintStrip.i: src/HitScintStrip.cc.i
.PHONY : src/HitScintStrip.i

# target to preprocess a source file
src/HitScintStrip.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitScintStrip.cc.i
.PHONY : src/HitScintStrip.cc.i

src/HitScintStrip.s: src/HitScintStrip.cc.s
.PHONY : src/HitScintStrip.s

# target to generate assembly for a file
src/HitScintStrip.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitScintStrip.cc.s
.PHONY : src/HitScintStrip.cc.s

src/HitStrip.o: src/HitStrip.cc.o
.PHONY : src/HitStrip.o

# target to build an object file
src/HitStrip.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitStrip.cc.o
.PHONY : src/HitStrip.cc.o

src/HitStrip.i: src/HitStrip.cc.i
.PHONY : src/HitStrip.i

# target to preprocess a source file
src/HitStrip.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitStrip.cc.i
.PHONY : src/HitStrip.cc.i

src/HitStrip.s: src/HitStrip.cc.s
.PHONY : src/HitStrip.s

# target to generate assembly for a file
src/HitStrip.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/HitStrip.cc.s
.PHONY : src/HitStrip.cc.s

src/INIExtendedReader.o: src/INIExtendedReader.cc.o
.PHONY : src/INIExtendedReader.o

# target to build an object file
src/INIExtendedReader.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIExtendedReader.cc.o
.PHONY : src/INIExtendedReader.cc.o

src/INIExtendedReader.i: src/INIExtendedReader.cc.i
.PHONY : src/INIExtendedReader.i

# target to preprocess a source file
src/INIExtendedReader.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIExtendedReader.cc.i
.PHONY : src/INIExtendedReader.cc.i

src/INIExtendedReader.s: src/INIExtendedReader.cc.s
.PHONY : src/INIExtendedReader.s

# target to generate assembly for a file
src/INIExtendedReader.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIExtendedReader.cc.s
.PHONY : src/INIExtendedReader.cc.s

src/INIReader.o: src/INIReader.cc.o
.PHONY : src/INIReader.o

# target to build an object file
src/INIReader.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIReader.cc.o
.PHONY : src/INIReader.cc.o

src/INIReader.i: src/INIReader.cc.i
.PHONY : src/INIReader.i

# target to preprocess a source file
src/INIReader.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIReader.cc.i
.PHONY : src/INIReader.cc.i

src/INIReader.s: src/INIReader.cc.s
.PHONY : src/INIReader.s

# target to generate assembly for a file
src/INIReader.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/INIReader.cc.s
.PHONY : src/INIReader.cc.s

src/Parameterisation.o: src/Parameterisation.cc.o
.PHONY : src/Parameterisation.o

# target to build an object file
src/Parameterisation.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Parameterisation.cc.o
.PHONY : src/Parameterisation.cc.o

src/Parameterisation.i: src/Parameterisation.cc.i
.PHONY : src/Parameterisation.i

# target to preprocess a source file
src/Parameterisation.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Parameterisation.cc.i
.PHONY : src/Parameterisation.cc.i

src/Parameterisation.s: src/Parameterisation.cc.s
.PHONY : src/Parameterisation.s

# target to generate assembly for a file
src/Parameterisation.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Parameterisation.cc.s
.PHONY : src/Parameterisation.cc.s

src/PhysicsList.o: src/PhysicsList.cc.o
.PHONY : src/PhysicsList.o

# target to build an object file
src/PhysicsList.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PhysicsList.cc.o
.PHONY : src/PhysicsList.cc.o

src/PhysicsList.i: src/PhysicsList.cc.i
.PHONY : src/PhysicsList.i

# target to preprocess a source file
src/PhysicsList.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PhysicsList.cc.i
.PHONY : src/PhysicsList.cc.i

src/PhysicsList.s: src/PhysicsList.cc.s
.PHONY : src/PhysicsList.s

# target to generate assembly for a file
src/PhysicsList.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PhysicsList.cc.s
.PHONY : src/PhysicsList.cc.s

src/PositionSystem.o: src/PositionSystem.cc.o
.PHONY : src/PositionSystem.o

# target to build an object file
src/PositionSystem.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PositionSystem.cc.o
.PHONY : src/PositionSystem.cc.o

src/PositionSystem.i: src/PositionSystem.cc.i
.PHONY : src/PositionSystem.i

# target to preprocess a source file
src/PositionSystem.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PositionSystem.cc.i
.PHONY : src/PositionSystem.cc.i

src/PositionSystem.s: src/PositionSystem.cc.s
.PHONY : src/PositionSystem.s

# target to generate assembly for a file
src/PositionSystem.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PositionSystem.cc.s
.PHONY : src/PositionSystem.cc.s

src/PrimaryGeneratorAction.o: src/PrimaryGeneratorAction.cc.o
.PHONY : src/PrimaryGeneratorAction.o

# target to build an object file
src/PrimaryGeneratorAction.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PrimaryGeneratorAction.cc.o
.PHONY : src/PrimaryGeneratorAction.cc.o

src/PrimaryGeneratorAction.i: src/PrimaryGeneratorAction.cc.i
.PHONY : src/PrimaryGeneratorAction.i

# target to preprocess a source file
src/PrimaryGeneratorAction.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PrimaryGeneratorAction.cc.i
.PHONY : src/PrimaryGeneratorAction.cc.i

src/PrimaryGeneratorAction.s: src/PrimaryGeneratorAction.cc.s
.PHONY : src/PrimaryGeneratorAction.s

# target to generate assembly for a file
src/PrimaryGeneratorAction.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/PrimaryGeneratorAction.cc.s
.PHONY : src/PrimaryGeneratorAction.cc.s

src/RunAction.o: src/RunAction.cc.o
.PHONY : src/RunAction.o

# target to build an object file
src/RunAction.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/RunAction.cc.o
.PHONY : src/RunAction.cc.o

src/RunAction.i: src/RunAction.cc.i
.PHONY : src/RunAction.i

# target to preprocess a source file
src/RunAction.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/RunAction.cc.i
.PHONY : src/RunAction.cc.i

src/RunAction.s: src/RunAction.cc.s
.PHONY : src/RunAction.s

# target to generate assembly for a file
src/RunAction.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/RunAction.cc.s
.PHONY : src/RunAction.cc.s

src/ScintillationSystem.o: src/ScintillationSystem.cc.o
.PHONY : src/ScintillationSystem.o

# target to build an object file
src/ScintillationSystem.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ScintillationSystem.cc.o
.PHONY : src/ScintillationSystem.cc.o

src/ScintillationSystem.i: src/ScintillationSystem.cc.i
.PHONY : src/ScintillationSystem.i

# target to preprocess a source file
src/ScintillationSystem.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ScintillationSystem.cc.i
.PHONY : src/ScintillationSystem.cc.i

src/ScintillationSystem.s: src/ScintillationSystem.cc.s
.PHONY : src/ScintillationSystem.s

# target to generate assembly for a file
src/ScintillationSystem.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ScintillationSystem.cc.s
.PHONY : src/ScintillationSystem.cc.s

src/SensitiveCalo3D.o: src/SensitiveCalo3D.cc.o
.PHONY : src/SensitiveCalo3D.o

# target to build an object file
src/SensitiveCalo3D.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveCalo3D.cc.o
.PHONY : src/SensitiveCalo3D.cc.o

src/SensitiveCalo3D.i: src/SensitiveCalo3D.cc.i
.PHONY : src/SensitiveCalo3D.i

# target to preprocess a source file
src/SensitiveCalo3D.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveCalo3D.cc.i
.PHONY : src/SensitiveCalo3D.cc.i

src/SensitiveCalo3D.s: src/SensitiveCalo3D.cc.s
.PHONY : src/SensitiveCalo3D.s

# target to generate assembly for a file
src/SensitiveCalo3D.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveCalo3D.cc.s
.PHONY : src/SensitiveCalo3D.cc.s

src/SensitiveEnergy.o: src/SensitiveEnergy.cc.o
.PHONY : src/SensitiveEnergy.o

# target to build an object file
src/SensitiveEnergy.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveEnergy.cc.o
.PHONY : src/SensitiveEnergy.cc.o

src/SensitiveEnergy.i: src/SensitiveEnergy.cc.i
.PHONY : src/SensitiveEnergy.i

# target to preprocess a source file
src/SensitiveEnergy.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveEnergy.cc.i
.PHONY : src/SensitiveEnergy.cc.i

src/SensitiveEnergy.s: src/SensitiveEnergy.cc.s
.PHONY : src/SensitiveEnergy.s

# target to generate assembly for a file
src/SensitiveEnergy.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveEnergy.cc.s
.PHONY : src/SensitiveEnergy.cc.s

src/SensitiveScintStrip.o: src/SensitiveScintStrip.cc.o
.PHONY : src/SensitiveScintStrip.o

# target to build an object file
src/SensitiveScintStrip.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveScintStrip.cc.o
.PHONY : src/SensitiveScintStrip.cc.o

src/SensitiveScintStrip.i: src/SensitiveScintStrip.cc.i
.PHONY : src/SensitiveScintStrip.i

# target to preprocess a source file
src/SensitiveScintStrip.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveScintStrip.cc.i
.PHONY : src/SensitiveScintStrip.cc.i

src/SensitiveScintStrip.s: src/SensitiveScintStrip.cc.s
.PHONY : src/SensitiveScintStrip.s

# target to generate assembly for a file
src/SensitiveScintStrip.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveScintStrip.cc.s
.PHONY : src/SensitiveScintStrip.cc.s

src/SensitiveStrip.o: src/SensitiveStrip.cc.o
.PHONY : src/SensitiveStrip.o

# target to build an object file
src/SensitiveStrip.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveStrip.cc.o
.PHONY : src/SensitiveStrip.cc.o

src/SensitiveStrip.i: src/SensitiveStrip.cc.i
.PHONY : src/SensitiveStrip.i

# target to preprocess a source file
src/SensitiveStrip.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveStrip.cc.i
.PHONY : src/SensitiveStrip.cc.i

src/SensitiveStrip.s: src/SensitiveStrip.cc.s
.PHONY : src/SensitiveStrip.s

# target to generate assembly for a file
src/SensitiveStrip.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SensitiveStrip.cc.s
.PHONY : src/SensitiveStrip.cc.s

src/SteppingAction.o: src/SteppingAction.cc.o
.PHONY : src/SteppingAction.o

# target to build an object file
src/SteppingAction.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingAction.cc.o
.PHONY : src/SteppingAction.cc.o

src/SteppingAction.i: src/SteppingAction.cc.i
.PHONY : src/SteppingAction.i

# target to preprocess a source file
src/SteppingAction.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingAction.cc.i
.PHONY : src/SteppingAction.cc.i

src/SteppingAction.s: src/SteppingAction.cc.s
.PHONY : src/SteppingAction.s

# target to generate assembly for a file
src/SteppingAction.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingAction.cc.s
.PHONY : src/SteppingAction.cc.s

src/SteppingVerbose.o: src/SteppingVerbose.cc.o
.PHONY : src/SteppingVerbose.o

# target to build an object file
src/SteppingVerbose.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingVerbose.cc.o
.PHONY : src/SteppingVerbose.cc.o

src/SteppingVerbose.i: src/SteppingVerbose.cc.i
.PHONY : src/SteppingVerbose.i

# target to preprocess a source file
src/SteppingVerbose.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingVerbose.cc.i
.PHONY : src/SteppingVerbose.cc.i

src/SteppingVerbose.s: src/SteppingVerbose.cc.s
.PHONY : src/SteppingVerbose.s

# target to generate assembly for a file
src/SteppingVerbose.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/SteppingVerbose.cc.s
.PHONY : src/SteppingVerbose.cc.s

src/TFormat.o: src/TFormat.cc.o
.PHONY : src/TFormat.o

# target to build an object file
src/TFormat.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/TFormat.cc.o
.PHONY : src/TFormat.cc.o

src/TFormat.i: src/TFormat.cc.i
.PHONY : src/TFormat.i

# target to preprocess a source file
src/TFormat.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/TFormat.cc.i
.PHONY : src/TFormat.cc.i

src/TFormat.s: src/TFormat.cc.s
.PHONY : src/TFormat.s

# target to generate assembly for a file
src/TFormat.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/TFormat.cc.s
.PHONY : src/TFormat.cc.s

src/Trigger.o: src/Trigger.cc.o
.PHONY : src/Trigger.o

# target to build an object file
src/Trigger.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Trigger.cc.o
.PHONY : src/Trigger.cc.o

src/Trigger.i: src/Trigger.cc.i
.PHONY : src/Trigger.i

# target to preprocess a source file
src/Trigger.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Trigger.cc.i
.PHONY : src/Trigger.cc.i

src/Trigger.s: src/Trigger.cc.s
.PHONY : src/Trigger.s

# target to generate assembly for a file
src/Trigger.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/Trigger.cc.s
.PHONY : src/Trigger.cc.s

src/ini.o: src/ini.cc.o
.PHONY : src/ini.o

# target to build an object file
src/ini.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ini.cc.o
.PHONY : src/ini.cc.o

src/ini.i: src/ini.cc.i
.PHONY : src/ini.i

# target to preprocess a source file
src/ini.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ini.cc.i
.PHONY : src/ini.cc.i

src/ini.s: src/ini.cc.s
.PHONY : src/ini.s

# target to generate assembly for a file
src/ini.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/ini.cc.s
.PHONY : src/ini.cc.s

src/z_SSBosonPhysics.o: src/z_SSBosonPhysics.cc.o
.PHONY : src/z_SSBosonPhysics.o

# target to build an object file
src/z_SSBosonPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSBosonPhysics.cc.o
.PHONY : src/z_SSBosonPhysics.cc.o

src/z_SSBosonPhysics.i: src/z_SSBosonPhysics.cc.i
.PHONY : src/z_SSBosonPhysics.i

# target to preprocess a source file
src/z_SSBosonPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSBosonPhysics.cc.i
.PHONY : src/z_SSBosonPhysics.cc.i

src/z_SSBosonPhysics.s: src/z_SSBosonPhysics.cc.s
.PHONY : src/z_SSBosonPhysics.s

# target to generate assembly for a file
src/z_SSBosonPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSBosonPhysics.cc.s
.PHONY : src/z_SSBosonPhysics.cc.s

src/z_SSDecayPhysics.o: src/z_SSDecayPhysics.cc.o
.PHONY : src/z_SSDecayPhysics.o

# target to build an object file
src/z_SSDecayPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSDecayPhysics.cc.o
.PHONY : src/z_SSDecayPhysics.cc.o

src/z_SSDecayPhysics.i: src/z_SSDecayPhysics.cc.i
.PHONY : src/z_SSDecayPhysics.i

# target to preprocess a source file
src/z_SSDecayPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSDecayPhysics.cc.i
.PHONY : src/z_SSDecayPhysics.cc.i

src/z_SSDecayPhysics.s: src/z_SSDecayPhysics.cc.s
.PHONY : src/z_SSDecayPhysics.s

# target to generate assembly for a file
src/z_SSDecayPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSDecayPhysics.cc.s
.PHONY : src/z_SSDecayPhysics.cc.s

src/z_SSHadronPhysics.o: src/z_SSHadronPhysics.cc.o
.PHONY : src/z_SSHadronPhysics.o

# target to build an object file
src/z_SSHadronPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSHadronPhysics.cc.o
.PHONY : src/z_SSHadronPhysics.cc.o

src/z_SSHadronPhysics.i: src/z_SSHadronPhysics.cc.i
.PHONY : src/z_SSHadronPhysics.i

# target to preprocess a source file
src/z_SSHadronPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSHadronPhysics.cc.i
.PHONY : src/z_SSHadronPhysics.cc.i

src/z_SSHadronPhysics.s: src/z_SSHadronPhysics.cc.s
.PHONY : src/z_SSHadronPhysics.s

# target to generate assembly for a file
src/z_SSHadronPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSHadronPhysics.cc.s
.PHONY : src/z_SSHadronPhysics.cc.s

src/z_SSIonPhysics.o: src/z_SSIonPhysics.cc.o
.PHONY : src/z_SSIonPhysics.o

# target to build an object file
src/z_SSIonPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSIonPhysics.cc.o
.PHONY : src/z_SSIonPhysics.cc.o

src/z_SSIonPhysics.i: src/z_SSIonPhysics.cc.i
.PHONY : src/z_SSIonPhysics.i

# target to preprocess a source file
src/z_SSIonPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSIonPhysics.cc.i
.PHONY : src/z_SSIonPhysics.cc.i

src/z_SSIonPhysics.s: src/z_SSIonPhysics.cc.s
.PHONY : src/z_SSIonPhysics.s

# target to generate assembly for a file
src/z_SSIonPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSIonPhysics.cc.s
.PHONY : src/z_SSIonPhysics.cc.s

src/z_SSLeptonPhysics.o: src/z_SSLeptonPhysics.cc.o
.PHONY : src/z_SSLeptonPhysics.o

# target to build an object file
src/z_SSLeptonPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSLeptonPhysics.cc.o
.PHONY : src/z_SSLeptonPhysics.cc.o

src/z_SSLeptonPhysics.i: src/z_SSLeptonPhysics.cc.i
.PHONY : src/z_SSLeptonPhysics.i

# target to preprocess a source file
src/z_SSLeptonPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSLeptonPhysics.cc.i
.PHONY : src/z_SSLeptonPhysics.cc.i

src/z_SSLeptonPhysics.s: src/z_SSLeptonPhysics.cc.s
.PHONY : src/z_SSLeptonPhysics.s

# target to generate assembly for a file
src/z_SSLeptonPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSLeptonPhysics.cc.s
.PHONY : src/z_SSLeptonPhysics.cc.s

src/z_SSNeutronPhysics.o: src/z_SSNeutronPhysics.cc.o
.PHONY : src/z_SSNeutronPhysics.o

# target to build an object file
src/z_SSNeutronPhysics.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSNeutronPhysics.cc.o
.PHONY : src/z_SSNeutronPhysics.cc.o

src/z_SSNeutronPhysics.i: src/z_SSNeutronPhysics.cc.i
.PHONY : src/z_SSNeutronPhysics.i

# target to preprocess a source file
src/z_SSNeutronPhysics.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSNeutronPhysics.cc.i
.PHONY : src/z_SSNeutronPhysics.cc.i

src/z_SSNeutronPhysics.s: src/z_SSNeutronPhysics.cc.s
.PHONY : src/z_SSNeutronPhysics.s

# target to generate assembly for a file
src/z_SSNeutronPhysics.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSNeutronPhysics.cc.s
.PHONY : src/z_SSNeutronPhysics.cc.s

src/z_SSPhysicsList.o: src/z_SSPhysicsList.cc.o
.PHONY : src/z_SSPhysicsList.o

# target to build an object file
src/z_SSPhysicsList.cc.o:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSPhysicsList.cc.o
.PHONY : src/z_SSPhysicsList.cc.o

src/z_SSPhysicsList.i: src/z_SSPhysicsList.cc.i
.PHONY : src/z_SSPhysicsList.i

# target to preprocess a source file
src/z_SSPhysicsList.cc.i:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSPhysicsList.cc.i
.PHONY : src/z_SSPhysicsList.cc.i

src/z_SSPhysicsList.s: src/z_SSPhysicsList.cc.s
.PHONY : src/z_SSPhysicsList.s

# target to generate assembly for a file
src/z_SSPhysicsList.cc.s:
	$(MAKE) -f CMakeFiles/Delacour.dir/build.make CMakeFiles/Delacour.dir/src/z_SSPhysicsList.cc.s
.PHONY : src/z_SSPhysicsList.cc.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... Delacour"
	@echo "... GCD_v16"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... Delacour.o"
	@echo "... Delacour.i"
	@echo "... Delacour.s"
	@echo "... src/BoxUnion.o"
	@echo "... src/BoxUnion.i"
	@echo "... src/BoxUnion.s"
	@echo "... src/BoxVolume.o"
	@echo "... src/BoxVolume.i"
	@echo "... src/BoxVolume.s"
	@echo "... src/Calo3DSystem.o"
	@echo "... src/Calo3DSystem.i"
	@echo "... src/Calo3DSystem.s"
	@echo "... src/DetectorConstruction.o"
	@echo "... src/DetectorConstruction.i"
	@echo "... src/DetectorConstruction.s"
	@echo "... src/DetectorMessenger.o"
	@echo "... src/DetectorMessenger.i"
	@echo "... src/DetectorMessenger.s"
	@echo "... src/Directions.o"
	@echo "... src/Directions.i"
	@echo "... src/Directions.s"
	@echo "... src/EnergySystem.o"
	@echo "... src/EnergySystem.i"
	@echo "... src/EnergySystem.s"
	@echo "... src/EventAction.o"
	@echo "... src/EventAction.i"
	@echo "... src/EventAction.s"
	@echo "... src/EventData.o"
	@echo "... src/EventData.i"
	@echo "... src/EventData.s"
	@echo "... src/Geo3D.o"
	@echo "... src/Geo3D.i"
	@echo "... src/Geo3D.s"
	@echo "... src/Geometry.o"
	@echo "... src/Geometry.i"
	@echo "... src/Geometry.s"
	@echo "... src/HitCalo3D.o"
	@echo "... src/HitCalo3D.i"
	@echo "... src/HitCalo3D.s"
	@echo "... src/HitEnergy.o"
	@echo "... src/HitEnergy.i"
	@echo "... src/HitEnergy.s"
	@echo "... src/HitScintStrip.o"
	@echo "... src/HitScintStrip.i"
	@echo "... src/HitScintStrip.s"
	@echo "... src/HitStrip.o"
	@echo "... src/HitStrip.i"
	@echo "... src/HitStrip.s"
	@echo "... src/INIExtendedReader.o"
	@echo "... src/INIExtendedReader.i"
	@echo "... src/INIExtendedReader.s"
	@echo "... src/INIReader.o"
	@echo "... src/INIReader.i"
	@echo "... src/INIReader.s"
	@echo "... src/Parameterisation.o"
	@echo "... src/Parameterisation.i"
	@echo "... src/Parameterisation.s"
	@echo "... src/PhysicsList.o"
	@echo "... src/PhysicsList.i"
	@echo "... src/PhysicsList.s"
	@echo "... src/PositionSystem.o"
	@echo "... src/PositionSystem.i"
	@echo "... src/PositionSystem.s"
	@echo "... src/PrimaryGeneratorAction.o"
	@echo "... src/PrimaryGeneratorAction.i"
	@echo "... src/PrimaryGeneratorAction.s"
	@echo "... src/RunAction.o"
	@echo "... src/RunAction.i"
	@echo "... src/RunAction.s"
	@echo "... src/ScintillationSystem.o"
	@echo "... src/ScintillationSystem.i"
	@echo "... src/ScintillationSystem.s"
	@echo "... src/SensitiveCalo3D.o"
	@echo "... src/SensitiveCalo3D.i"
	@echo "... src/SensitiveCalo3D.s"
	@echo "... src/SensitiveEnergy.o"
	@echo "... src/SensitiveEnergy.i"
	@echo "... src/SensitiveEnergy.s"
	@echo "... src/SensitiveScintStrip.o"
	@echo "... src/SensitiveScintStrip.i"
	@echo "... src/SensitiveScintStrip.s"
	@echo "... src/SensitiveStrip.o"
	@echo "... src/SensitiveStrip.i"
	@echo "... src/SensitiveStrip.s"
	@echo "... src/SteppingAction.o"
	@echo "... src/SteppingAction.i"
	@echo "... src/SteppingAction.s"
	@echo "... src/SteppingVerbose.o"
	@echo "... src/SteppingVerbose.i"
	@echo "... src/SteppingVerbose.s"
	@echo "... src/TFormat.o"
	@echo "... src/TFormat.i"
	@echo "... src/TFormat.s"
	@echo "... src/Trigger.o"
	@echo "... src/Trigger.i"
	@echo "... src/Trigger.s"
	@echo "... src/ini.o"
	@echo "... src/ini.i"
	@echo "... src/ini.s"
	@echo "... src/z_SSBosonPhysics.o"
	@echo "... src/z_SSBosonPhysics.i"
	@echo "... src/z_SSBosonPhysics.s"
	@echo "... src/z_SSDecayPhysics.o"
	@echo "... src/z_SSDecayPhysics.i"
	@echo "... src/z_SSDecayPhysics.s"
	@echo "... src/z_SSHadronPhysics.o"
	@echo "... src/z_SSHadronPhysics.i"
	@echo "... src/z_SSHadronPhysics.s"
	@echo "... src/z_SSIonPhysics.o"
	@echo "... src/z_SSIonPhysics.i"
	@echo "... src/z_SSIonPhysics.s"
	@echo "... src/z_SSLeptonPhysics.o"
	@echo "... src/z_SSLeptonPhysics.i"
	@echo "... src/z_SSLeptonPhysics.s"
	@echo "... src/z_SSNeutronPhysics.o"
	@echo "... src/z_SSNeutronPhysics.i"
	@echo "... src/z_SSNeutronPhysics.s"
	@echo "... src/z_SSPhysicsList.o"
	@echo "... src/z_SSPhysicsList.i"
	@echo "... src/z_SSPhysicsList.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

