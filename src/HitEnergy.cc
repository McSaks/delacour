#include "HitEnergy.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

G4Allocator<HitEnergy> hitAllocatorEnergy;


HitEnergy::HitEnergy() {}

HitEnergy::~HitEnergy() {}

HitEnergy::HitEnergy(const HitEnergy& right)
  : G4VHit()
{
  trackID   = right.trackID;
  edep      = right.edep;
  pos       = right.pos;
}

const HitEnergy& HitEnergy::operator=(const HitEnergy& right)
{
  trackID   = right.trackID;
  edep      = right.edep;
  pos       = right.pos;
  return *this;
}

G4int HitEnergy::operator==(const HitEnergy& right) const
{
  return (this==&right) ? 1 : 0;
}

void HitEnergy::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pos);
    circle.SetScreenSize(2.);
    circle.SetFillStyle(G4Circle::filled);
    G4Color color(.3,.3,0.);
    G4VisAttributes attribs(color);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

void HitEnergy::Print()
{
  G4cout << "  track ID:       " << trackID << '\n';
  G4cout << "  energy deposit: " << G4BestUnit(edep,"Energy") << '\n';
  G4cout << "  position:       " << G4BestUnit(pos,"Length") << G4endl;
}
