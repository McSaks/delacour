#include "EventData.hh"
#include "DetectorConstruction.hh"
#include "SensitiveScintStrip.hh"
#include "SensitiveCalo3D.hh"
#include "FOR.hh"

G4double TimeUnit::kNotActuated = -100;

/* * * * * * * * *\
|* PositionData  *|
\* * * * * * * * */

PositionData::PositionData(EventData* evd_)
  : evd(evd_), C(evd->GetDetectorConstruction()->CLayerSiY.nlayers)
  , CDmidi(1), CC1(evd->GetDetectorConstruction()->CC1LayerSiY.nlayers)
{ }

void vclear(TrackUnits& t) { vFOR(LayerUnit, t, it) it->Y.clear(), it->Z.clear(); }
void PositionData::Clear() { vclear(C);vclear(CDmidi);vclear(CC1);trackPoints.clear(); }

TrackUnit::TrackUnit(const TrackPoint& p)
  : stripNo(p.stripNo), edep(p.edep)
{ }

TrackUnit& TrackUnit::operator=(const TrackPoint& p)
{
  stripNo = p.stripNo;
  edep = p.edep;
  return *this;
}

TrackUnit& TrackUnit::operator=(const TrackUnit& p)
{
  stripNo = p.stripNo;
  edep = p.edep;
  return *this;
}


/* * * * * * * * *\
|*   TimeData    *|
\* * * * * * * * */

TimeData::TimeData(EventData* evd_)
  : evd(evd_)
{ }

/*TimeUnit::TimeUnit(const TimePoint& p)
  : actuationtime(p.GetStartTime()), edep(p.GetAmplitude())
{
  if(actuationtime <= 0)
    actuationtime = kNotActuated;
}*/

/*TimeUnit& TimeUnit::operator=(const TimePoint& p)
{
  actuationtime = p.GetStartTime(); edep = p.GetAmplitude();
  if(actuationtime <= 0)
    actuationtime = kNotActuated;
  return *this;
}*/

/*TimeUnit& TimeUnit::operator=(const TimeUnit& p)
{
  //actuationtime = p.actuationtime; edep = p.edep;
  return *this;
}*/

#if 0
TimeUnit& TimeUnit::operator+=(const TimeUnit& p)
{
  /*if(     actuationtime==kNotActuated  // if any of them are
     || p.actuationtime==kNotActuated //  not actuated or
     || !operator==(p))              //   they differ in time:
    return *this;                   //  do nothing
  actuationtime += p.actuationtime; actuationtime/=2;
  edep += p.edep;*/
  return *this;
}

G4bool TimeUnit::operator<(const TimeUnit& right)
{
  // "Bad" units must be at end on sorting:
  /*if(edep <= 0) return false;
  if(actuationtime == kNotActuated) return false;
  if(right.edep <= 0) return true;
  if(right.actuationtime == kNotActuated) return true;
  return actuationtime<right.actuationtime;*/ return true;
}

G4bool TimeUnit::operator==(const TimeUnit& right)
{
  //if(actuationtime-right.actuationtime > 100*ps) return false;
  //if(right.actuationtime-actuationtime > 100*ps) return false;
  return true;
}
#endif


/* * * * * * * * *\
|*  EnergyData   *|
\* * * * * * * * */

EnergyData::EnergyData(EventData* evd_)
  : evd(evd_), CC1(evd->GetDetectorConstruction()->CC1LayerSiY.nlayers)
{ }

EnergyUnit::EnergyUnit(const EnergyPoint& p)
  : edep(p.edep)
{ }

EnergyUnit& EnergyUnit::operator=(const EnergyPoint& p)
{
  edep = p.edep;
  return *this;
}

EnergyUnit& EnergyUnit::operator=(const EnergyUnit& p)
{
  edep = p.edep;
  return *this;
}



/* * * * * * * * *\
|*  Calo3DData   *|
\* * * * * * * * */

Calo3DData::Calo3DData(EventData* evd_)
  : evd(evd_)
  , active(false)
{ }





/* * * * * * * * *\
|*   EventData   *|
\* * * * * * * * */

EventData::EventData(DetectorConstruction* d, G4String ofile)
  : det(d), positionData(this), timeData(this), energyData(this), calo3dData(this)
  , _ok(true), use(true)
  , write_structured(true), write_trackPoints(false)//, write_position_energy(false)
  , outfilename(ofile)
{
  outfile.open(outfilename, std::ofstream::out|std::ofstream::app);
  if(!outfile) {
    G4cerr << "File \"" << outfilename << "\" cannot be opened" << G4endl; Reject();
  } else G4cout << "File \"" << outfilename << "\" opened" << G4endl;
  outfile.setf(std::ios::fixed);
}

EventData::~EventData()
{
  outfile << G4endl;
  outfile.close();
  G4cout << "File \"" << outfilename << "\" closed" << G4endl;
}

void EventData::Clear()
{
  positionData .Clear();
  timeData     .Clear();
  energyData   .Clear();
}

void EventData::WritePD(const TrackUnits& tu)
{
  vFORc(LayerUnit, tu, layer) {
    outfile << layer->Y.size() << " ";
    vFORc(TrackUnit, layer->Y, unit) {
      outfile << unit->stripNo << " " << unit->edep/MeV << " ";
    };
    outfile << backspace << "\n";
    outfile << layer->Z.size() << " ";
    vFORc(TrackUnit, layer->Z, unit) {
      outfile << unit->stripNo << " " << unit->edep/MeV << " ";
    };
    outfile << backspace << "\n";
  };
}

void EventData::WriteTD(const TimeUnit& tu)
{
 switch(scintstripoutput) {
  case kScintStrip_array : {
    ScintStripLayerFORc(*(tu.data), p)
      p->edep
        ? outfile << p->edep/MeV << " "
        : outfile << "0 ";
    outfile << backspace << "\n";
    return;
  }
  case kScintStrip_tree : {
    if( tu.data->sensitive->HasSignal() )
      outfile << tu.data->sensitive->earliest /ns << " "
              << tu.data->sensitive->latest   /ns << "  ";
    else
      outfile << "0 0  ";
    G4int ss, pp, tt;
    std::vector<G4int>::iterator _s, _p, _t;
    vector<G4int> s_nonzero, p_nonzero, t_nonzero;
    s_nonzero.reserve(tu.data->s_nbins);
    p_nonzero.reserve(tu.data->p_nbins);
    t_nonzero.reserve(tu.data->t_nbins);
    G4double et;
    G4bool hasnot;
    dFORs(ss, 0, tu.data->s_nbins) { hasnot = true;
     dFORs(tt, 0, tu.data->t_nbins) {
      dFORs(pp, 0, tu.data->p_nbins) {
        if( (*tu.data)(ss,pp,tt).edep > 0 and hasnot)
          s_nonzero.push_back(ss), hasnot = false;
      }
     }
    }
    outfile << tu.data->sensitive->Etot/MeV << " " << s_nonzero.size() << " ";
    vdFOR(s_nonzero, _s) {
      t_nonzero.clear();
      dFORs(tt, 0, tu.data->t_nbins) { hasnot = true;
       dFORs(pp, 0, tu.data->p_nbins) {
         if( (*tu.data)(*_s,pp,tt).edep > 0 and hasnot)
           t_nonzero.push_back(tt), hasnot = false;
       }
      }
      outfile <<*_s << " " << tu.data->sensitive->stripEtot->at(*_s)/MeV << " "
              << t_nonzero.size() << " ";
      vdFOR(t_nonzero, _t) {
        p_nonzero.clear();
        et = 0.;
        dFORs(pp, 0, tu.data->p_nbins) { hasnot = true;
          if( (*tu.data)(*_s,pp,*_t).edep > 0 and hasnot)
            p_nonzero.push_back(pp), hasnot = false, et += (*tu.data)(*_s,pp,*_t);
        }
        outfile << *_t << " " << et/MeV << " " << p_nonzero.size() << " ";
        vdFOR(p_nonzero, _p) {
          outfile << *_p << " " << (*tu.data)(*_s,*_p,*_t)/MeV << " ";
        }
      }
    }
    outfile << backspace << "\n";
    return;
  }
  case kScintStrip_PMSignals : {
    ScintStripSignals signals = tu.data->GetSignal();
    G4double L = tu.data->s_max - tu.data->s_min;
    G4double v = tu.data->speedoflight;
    G4double L_per_v = L/v;
    G4double t0, t1, t2;
    FORs(i, 0, tu.data->s_nbins) {
      t1 = signals.one.GetActuationTime(i);
      t2 = signals.two.GetActuationTime(i);
      if( t1 == SensitiveScintStrip::kNoSignal ||
          t2 == SensitiveScintStrip::kNoSignal )
        outfile << "0 0 ";
      else {
        t0 = .5 * ( t1 + t2 - L_per_v );
//Disp(""); t0 = (t1<t2)?t1:t2;
        outfile << t0/ns << " ";
        outfile << tu.data->sensitive->stripEtot->at(i)/MeV << " ";
      }
    }
    outfile << backspace << "\n";
    return;
  }
 }
  
}

void EventData::WriteED(const EnergyUnit& eu)
{
  outfile << eu.edep/MeV << " ";
}

void EventData::Write3D(const Calo3DUnit& cu)
{
  Edep3D& cu_array = *cu.array;
  outfile << cu.edep/MeV << " ";
  //outfile << cu_array(0,0,0)/MeV << " ";
  G4int x, y, z;
  std::vector<G4int>::iterator _x, _y, _z;
  vector<G4int> x_nonzero, y_nonzero, z_nonzero;
  G4bool hasnot;
  #if 0
  BEGIN_SCOPE
    outfile << "[X ";
    dFORs(x, 0, x_nbins) {
     outfile << "[Y ";
     y_nbins = cu_array.GetItemCountY(x);
     dFORs(y, 0, y_nbins) {
      outfile << "[Z ";
      z_nbins = cu_array.GetItemCountZ(x);
      dFORs(z, 0, z_nbins) {
        outfile << cu_array(x,y,z)/MeV << " ";
      }
      outfile << "Z] ";
     }
      outfile << "Y] ";
    }
    outfile << "X]" << G4endl;
    return;
  END_SCOPE
  #endif
  G4bool segmented = GetDetectorConstruction()->CC2_is_segmented;
  outfile << ( segmented ? "+ " : "-\n" );
  voidensure(segmented);
  G4int x_nbins = cu_array.GetItemCountX(), y_nbins = 0, z_nbins = 0;
  x_nonzero.reserve(x_nbins);
  y_nonzero.reserve(y_nbins);
  z_nonzero.reserve(z_nbins);
  dFORs(x, 0, x_nbins) { hasnot = true;
   y_nbins = cu_array.GetItemCountY(x);
   dFORs(y, 0, y_nbins) {
    z_nbins = cu_array.GetItemCountZ(x);
    dFORs(z, 0, z_nbins) {
      if( cu_array(x,y,z) > 0 and hasnot)
        x_nonzero.push_back(x), hasnot = false;
    }
   }
  }
  outfile << x_nonzero.size() << " ";
  vdFOR(x_nonzero, _x) {
    y_nonzero.clear();
    y_nbins = cu_array.GetItemCountY(*_x);
    dFORs(y, 0, y_nbins) { hasnot = true;
    z_nbins = cu_array.GetItemCountZ(*_x);
     dFORs(z, 0, z_nbins) {
       if( cu_array(*_x,y,z) > 0 and hasnot)
         y_nonzero.push_back(y), hasnot = false;
     }
    }
    outfile << *_x << " " << y_nonzero.size() << " ";
    vdFOR(y_nonzero, _y) {
      z_nonzero.clear();
      z_nbins = cu_array.GetItemCountZ(*_x);
      dFORs(z, 0, z_nbins) { hasnot = true;
        if( cu_array(*_x,*_y,z) > 0 and hasnot)
          z_nonzero.push_back(z), hasnot = false;
      }
      outfile << *_y << " " << z_nonzero.size() << " ";
      vdFOR(z_nonzero, _z) {
        outfile << *_z << " " << cu_array(*_x,*_y,*_z)/MeV << " ";
      }
    }
  }
  outfile << backspace << "\n";

}

void EventData::Write()
{
 if(ok()) {
  WritePD(positionData.C);
  WriteRecSep();
  WritePD(positionData.CC1);
  AfterPD();
  WriteRecSep();
  WriteTD(timeData.topAC);
  WriteTD(timeData.sideAC_left);
  WriteTD(timeData.sideAC_front);
  WriteTD(timeData.sideAC_right);
  WriteTD(timeData.sideAC_back);
  WriteTD(timeData.S1);
  WriteTD(timeData.S2);
  WriteTD(timeData.S3);
  WriteTD(timeData.S4);
  AfterTD();
  WriteRecSep();
  vFORc(EnergyUnit, energyData.CC1, it) WriteED(*it);
  AfterED();
  if(true) //if(calo3dData.active)
    Write3D(calo3dData.CC2),
    After3D();
  else
    WriteED(energyData.CC2),
    AfterED();
  WriteEvtSep();
 }
}
