#include "SensitiveCalo3D.hh"
#include "globals.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "Parameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4Box.hh"
#include "G4VPVParameterisation.hh"
#include "G4VSolid.hh"
#include "FOR.hh"

class Parameterisation;
class G4VPVParameterisation;
class G4PVParameterised;
class G4VSolid;
class G4Box;



SensitiveCalo3D::SensitiveCalo3D(G4String name, DetectorConstruction* aDet, G4VPhysicalVolume* pv,
       Calo3DSystem* sys, G4int xrows, const vector<G4int>& yrows, const vector<G4int>& zrows, const vector<G4double>& ywidths, const vector<G4double>& zwidths, EDetectorType type)
: SensitiveDetector(name)
, system(sys)
, hitsCollection(NULL)
, nitemsX(xrows)
, nitemsY(yrows)
, nitemsZ(zrows)
, widthY(ywidths)
, widthZ(zwidths)
, etot(0.)
{
  detConstr = aDet;
  SDtype = type;
  fPV = pv;
  detPart = 0;
  fCopyNo = 0;
  G4cout << "Sensitive " << SensitiveDetectorName << " constructed" << G4endl;
  G4String tmp = name(1,1);
  tmp.toUpper();
  collectionName.insert( G4String("hitsCollection") + tmp + G4String(name(2, name.size() - 2)) );
  IF(fPV)
    IF_ fPV->IsParameterised() THEN
       Parameterisation* parampampam = (Parameterisation*) ((G4PVParameterised*)fPV)->GetParameterisation();
       fpos  = parampampam->GetPosition(fCopyNo);
       fdims = parampampam->GetDimentions(fCopyNo);
    ELSE
       fpos  = fPV->GetTranslation();
       G4Box* box = (G4Box*) fPV->GetLogicalVolume()->GetSolid();
       fdims.set(box->GetXHalfLength(), box->GetYHalfLength(), box->GetZHalfLength());
    END (if IsParameterised)
  END (if fPV)
  
  system->etot = &etot;
  system->array = &dEArray;
  dEArray.nitemsX = nitemsX, dEArray.nitemsY = nitemsY, dEArray.nitemsZ = nitemsZ;
  dEArray.Reserve(xrows, yrows, zrows);
}


/*SensitiveStrip::SensitiveStrip(G4String name, DetectorConstruction* aDet, G4int nstrips,
                               G4ThreeVector pos, G4ThreeVector dims, EAxis ax = kYAxis)
:G4VSensitiveDetector(name)
,fNstrips(nstrips)
,detConstr(aDet)
,fAxis(ax)
,fpos(pos)
,fdims(dims)
{
  collectionName.insert("hitsCollection"+name(1,1).ToUpper()+name(2,name.size()-2));
}*/

SensitiveCalo3D::~SensitiveCalo3D() { }

void SensitiveCalo3D::Initialize(G4HCofThisEvent*)
{
  clear();
  //hitsCollection = new HitsCollectionCalo3D
  //                        (SensitiveDetectorName,collectionName[0]); 
  //static G4int HCID = -1;
  //if(HCID<0)
  //{ HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); }
  //HCE->AddHitsCollection( HCID, hitsCollection );
}

#define Calo3D_FIND_CENTER() ( x - dx + dx/n * (2*item_index + 1) )
#define Calo3D_FIND_INDEX()  ( G4int(( x - min) / (max - min) * n) )

#if 0
G4double SensitiveCalo3D::GetItemCenter(const G4int& item_index, const EItemDir& dir) const
{
  G4double x, dx;
  G4int n;
  SWITCH(dir)
    CASE kXDir:
      x  = fpos.x();
      dx = fdims.x();
      n  = nitemsX;
    CASE kYDir:
      x  = fpos.y();
      dx = fdims.y();
      n  = nitemsY.at();
    CASE kZDir:
      x  = fpos.z();
      dx = fdims.z();
      n  = nitemsZ;
  END_SWITCH
  return Calo3D_FIND_CENTER();
}
#endif

G4ThreeVector SensitiveCalo3D::GetItemCenter(const Index3D& i) const
{
  G4ThreeVector p;
  G4double x, dx;
  G4int n, item_index;
  
  x  = fpos.x();
  dx = fdims.x();
  n  = nitemsX;
  item_index = i.x;
  p.setX( Calo3D_FIND_CENTER() );
  
  x  = fpos.y();
  dx = fdims.y();
  n  = nitemsY.at(i.x);
  item_index = i.y;
  p.setY( Calo3D_FIND_CENTER() );
  
  x  = fpos.z();
  dx = fdims.z();
  n  = nitemsZ.at(i.x);
  item_index = i.z;
  p.setZ( Calo3D_FIND_CENTER() );
  
  return p;
}

#if 0
G4int SensitiveCalo3D::ComputeItem(const G4double& x, const EItemDir& dir) const
{
  G4double min, max;
  G4int n;
  SWITCH(dir)
    CASE kXDir:
      min = fpos.x() - fdims.x();
      max = fpos.x() + fdims.x();
      n  = nitemsX;
    CASE kYDir:
      min = fpos.y() - fdims.y();
      max = fpos.y() + fdims.y();
      n  = nitemsY;
    CASE kZDir:
      min = fpos.z() - fdims.z();
      max = fpos.z() + fdims.z();
      n  = nitemsZ;
  END_SWITCH
  if(min > x || x > max) return kOutOfVolume;
  G4int item_index = Calo3D_FIND_INDEX();
  if(item_index == n) item_index = n-1;
  return item_index;
}
#endif

Index3D SensitiveCalo3D::ComputeItem(const G4ThreeVector& r) const
{
  G4double min, max, x, halfwidth;
  G4int n;
  Index3D index;
  
  min = fpos.x() - fdims.x();
  max = fpos.x() + fdims.x();
  x = r.x();
  if(min > x || x > max) return Index3D(kOutOfVolume);
  n  = nitemsX;
  index.x = Calo3D_FIND_INDEX();
  if(index.x == n) --index.x;
  
  halfwidth = widthY.at(index.x)/2;
  min = fpos.y() - halfwidth;
  max = fpos.y() + halfwidth;
  x = r.y();
  if(min > x || x > max) return Index3D(kOutOfVolume);
  n  = nitemsY.at(index.x);
  index.y = Calo3D_FIND_INDEX();
  if(index.y == n) --index.y;
  
  halfwidth = widthZ.at(index.x)/2;
  min = fpos.z() - halfwidth;
  max = fpos.z() + halfwidth;
  x = r.z();
  if(min > x || x > max) return Index3D(kOutOfVolume);
  n  = nitemsZ.at(index.x);
  index.z = Calo3D_FIND_INDEX();
  if(index.z == n) --index.z;
  return index;
}

G4double SensitiveCalo3D::GetItemDims(G4int layer, const EItemDir& dir) const
{
  SWITCH(dir)
    case kXDir:
      return fdims.x()/nitemsX;
    case kYDir:
      return fdims.y()/nitemsY.at(layer);
    case kZDir:
      return fdims.z()/nitemsZ.at(layer);
    default:
      return 0;
  END_SWITCH
}

G4ThreeVector SensitiveCalo3D::GetItemDims(G4int layer) const
{
  return G4ThreeVector( fdims.x()/nitemsX, fdims.y()/nitemsY.at(layer), fdims.z()/nitemsZ.at(layer) );
}

//G4double SensitiveCalo3D::GetHalfWidthY(G4int layer) const



G4bool SensitiveCalo3D::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double edep = aStep->GetTotalEnergyDeposit();
  if(edep==0.) return false;
  etot += edep;
  //dEArray
  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4ThreeVector hitpos = preStepPoint->GetPosition();
  G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
  //HitStrip* newHit = new HitStrip();
  //newHit->SetTrackID(aStep->GetTrack()->GetTrackID());
  //newHit->SetPos(hitpos);
  //newHit->SetEdep(edep);
  //Index3D itemIndex = ComputeItem(hitpos);
  G4int itemIndex = theTouchable->GetCopyNumber();
  
  dEArray.Add(itemIndex, edep);
  
  return true;
}

void SensitiveCalo3D::EndOfEvent(G4HCofThisEvent*) { }
void SensitiveCalo3D::clear()
{
  dEArray.Reset();
  etot = 0.;
}



inline G4int Edep3D::GetTotalItemCount() const
{
  G4int total = 0;
  FORs(i, 0, nitemsX)
    total += nitemsY.at(i) * nitemsZ.at(i);
  return total;
}

void Edep3D::Reserve(G4int nx, const vector<G4int>& ny, const vector<G4int>& nz)
{
  clear();
  nitemsX = nx; nitemsY = ny; nitemsZ = nz;
  Reset();
}

void Edep3D::Reset()
{
  clear();
  FORs(i, 0, nitemsX) {
    z_default.assign  ( nitemsZ.at(i), 0 );
    yz_default.assign ( nitemsY.at(i), z_default );
    push_back(yz_default);
  }
}

G4int Edep3D::FlattenIndex(const Index3D& i) const
{
  if (i.x == -1 && i.y == -1 && i.z == -1) return -1;
  G4int nx = size(), nz, n;
  G4int c = 0;
  FORs(x, 0, nx - 1) {
    nz = nitemsZ.at(x);
    n = nz * nitemsY.at(x);
    if(i.x == x) {
      return c + nz*i.y + i.z;
    }
    c += n;
  }
  // Never to be reached:
  G4Exception(
    "Edep3D::FlattenIndex",
    "Fatal exception",
    FatalException,
    "Flattening failed.");
  return -1;
}

Index3D Edep3D::ThreedeeizeIndex(G4int j) const
{
  //Disp(j, "Flat index: ");
  if(j == -1) return Index3D(-1, -1, -1);
  G4int nx = size(), nz, n, iz;
  G4int c = 0;
  FORs(x, 0, nx) {
    nz = nitemsZ.at(x);
    n = nz * nitemsY.at(x);
    if(j < c + n) {
      iz = (j - c) % nz;
      return Index3D(x, (j - c - iz) / nz, iz);
    }
    c += n;
  }
  // Never to be reached:
  G4Exception(
    "Edep3D::ThreedeeizeIndex",
    "Fatal exception",
    FatalException,
    "3D-izing failed.");
  return Index3D(-1, -1, -1);
}
