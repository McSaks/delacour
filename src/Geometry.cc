#include "Geometry.hh"
#include "DetectorConstruction.hh"
#include "FOR.hh"
#include "G4UnitsTable.hh"
using namespace std;


template<typename T>
T front(const vector<T>& v)
{
  return v.size() ? v.front() : T();
}

Geometry::Geometry(DetectorConstruction* d, G4String ifile)
  : INIExtendedReader(ifile), det(d) { }

Geometry::~Geometry()
{ }

#include "G4Material.hh"
G4Material* Geometry::AlType(const G4int& type)
{
  switch(type) {
    case 16:
      return det->materAl16;
    case 48:
      return det->materAl48;
    case 108:
      return det->materAl108;
    default:
      return det->materVacuum;
  }
}

G4Material* Geometry::MatType(const G4String& type)
{
  WHICH
    INCASE type == "W" THEN
      return det->materW;
    INCASE type == "CsI" THEN
      return det->materCsI;
    INCASE type == "BGO" THEN
      return det->materBGO;
  END_WHICH
  return NULL;
}

vector<G4Material*> Geometry::AlType(const vector<G4int>& in)
{
  vector<G4Material*> out;
  vFORc(G4int, in, it)
    out.push_back(AlType(*it));
  return out;
}

vector<G4Material*> Geometry::MatType(vector<G4String>& in)
{
  vector<G4Material*> out;
  vFOR(G4String, in, it)
    out.push_back(MatType(*it));
  return out;
}


/* * * * * * * * * * * * * * * * * *\
|*  Read data from geometry file   *|
\* * * * * * * * * * * * * * * * * */

G4bool Geometry::Read()
BEGIN
  
  //ensure(infile);
  ValueUv tmpDuv;
  ValueIv tmpIv;
  ValueSv tmpSv;
  BoxVolume vol;
  G4double thickness, ywidth, zwidth, spreading;
  vector<G4double> thicknesses;
  G4int alType;
  vector<long> alTypes, numbers;
  vector<G4double> C_gaps, CC1_gaps_SC, CC1_gaps_CS;
  G4bool odd;
  register size_t ilayer;
  size_t nlayers;
  G4double pitchY, pitchZ;
  G4bool CC2_segmented;
  const G4double speed_of_light_in_vacuum = 2.99792458e+8 * m/s;
   
   
   G4bool global_supports = ReadB("world", "supports", true);
   
   ///////////////////////////////////
  //             world             //
 ///////////////////////////////////
   
   thickness = ReadU("world", "semiheight", 4*m);
   ywidth = ReadU("world", "y-width", 4*m);
   zwidth = ReadU("world", "z-width", 4*m);
   det->world.name = "World";
   det->world.pos.set(0, 0, 0);
   det->world.dims.set(thickness, ywidth/2, zwidth/2);
   det->world.SetMater(det->materVacuum);
   
   
   ///////////////////////////////////
  //             topAC             //
 ///////////////////////////////////
   
   G4bool topAC_supports = ReadB("topAC", "supports", true);
   
   // common:
   det->topAC.attr.scint.nstrips      = ReadI("topAC", "n-strips", 1);
   det->topAC.attr.scint.s_offset     = ReadU("topAC", "strip-offset", 0.);
   det->topAC.attr.scint.p_nbins      = ReadI("topAC", "p-bins", 1);
   det->topAC.attr.scint.t_nbins      = ReadI("topAC", "t-bins", 1);
   det->topAC.attr.scint.t_min        = ReadU("topAC", "t-min", 0*ns);
   det->topAC.attr.scint.t_max        = ReadU("topAC", "t-max", 1*s);
   det->topAC.attr.scint.speedoflight = ReadU("topAC", "speed-of-light", speed_of_light_in_vacuum);
   //det->topAC.attr.time.gain        = ReadD("topAC", "gain", 1.);
   //det->topAC.attr.time.risetime    = ReadU("topAC", "rise-time", 0.);
   //det->topAC.attr.time.falltime    = ReadU("topAC", "fall-time", 0.);
   
   // scintillator:
   det->topAC.name = "topAC";
   thickness = ReadU("topAC", "pSty-thickness", 0.);
   ywidth    = ReadU("topAC", "pSty-y-width", 0.);
   zwidth    = ReadU("topAC", "pSty-z-width", 0.);
   det->topAC.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->topAC.SetMother(det->world);
   det->topAC.SetMater(det->materPSty);
   
   // Al:
   det->topACSupportAl.name = "topACSupportAl";
   thickness = ReadU("topAC", "Al-thickness", 0.);
   ywidth    = ReadU("topAC", "Al-y-width", 0.);
   zwidth    = ReadU("topAC", "Al-z-width", 0.);
   alType    = ReadI("topAC", "Al-type", 0);
   det->topACSupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->topACSupportAl.SetMother(det->world);
   det->topACSupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->topACSupportTopCFRP.name = "topACSupportTopCFRP";
   thickness = ReadU("topAC", "CFRP-thickness", 0.);
   ywidth    = ReadU("topAC", "CFRP-y-width", 0.);
   zwidth    = ReadU("topAC", "CFRP-z-width", 0.);
   det->topACSupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->topACSupportTopCFRP.SetMother(det->world);
   det->topACSupportTopCFRP.SetMater(det->materCFRP);
   det->topACSupportBotCFRP = det->topACSupportTopCFRP;
   det->topACSupportBotCFRP.name = "topACSupportBotCFRP";
   
   // gap:
   G4double gap_topAC_C = ReadU("gaps", "AC--C", 0.);
   
   
   ///////////////////////////////////
  //               C               //
 ///////////////////////////////////
   
   G4bool C_supports = ReadB("C", "supports", true);
   
   // common:
   nlayers   = ReadI("C", "n-layers", 1);
   pitchY    = ReadU("C", "y-pitch", cm);
   pitchZ    = ReadU("C", "z-pitch", cm);
   spreading = ReadU("C", "spreading", -1.);
   C_gaps    = ReadUv("C", "gaps", nlayers, 0.);
   
   // Si (Y and Z):
   thicknesses = ReadUv("C", "Si-thickness", 2*nlayers, 0.);
   ywidth      = ReadU("C", "Si-y-width", 0.);
   zwidth      = ReadU("C", "Si-z-width", 0.);
   det->CLayerSiY.nlayers = nlayers;
   det->CLayerSiY.name = "CLayerSiY";
   det->CLayerSiY.mater = det->materSi;
   det->CLayerSiY.SetMother(det->world);
   det->CLayerSiY.attr.strip.spreading = spreading;
   det->CLayerSiY.attr.strip.nstrips = G4int(ywidth/pitchY);
   det->CLayerSiZ = det->CLayerSiY;
   det->CLayerSiZ.name = "CLayerSiZ";
   det->CLayerSiZ.attr.strip.nstrips = G4int(zwidth/pitchZ);
   dFORs(ilayer, 0, det->CLayerSiY.nlayers) {
     vol = det->CLayerSiY.DefaultChild();
     vol = det->CLayerSiY.DefaultChild();
     vol.name = det->CLayerSiY.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[2*ilayer]/2, ywidth/2, zwidth/2);
     det->CLayerSiY.Add(vol);
     vol = det->CLayerSiZ.DefaultChild();
     vol.name = det->CLayerSiZ.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[2*ilayer+1]/2, ywidth/2, zwidth/2);
     det->CLayerSiZ.Add(vol);
   }
   
   // W:
   G4double XoW = det->materW->GetRadlen();
   thicknesses = ReadUv("C", "W-thickness", nlayers, 0., XoW);
   ywidth      = ReadU("C", "W-y-width", 0., XoW);
   zwidth      = ReadU("C", "W-z-width", 0., XoW);
   det->CLayerW.nlayers = nlayers;
   det->CLayerW.name = "CLayerW";
   det->CLayerW.mater = det->materW;
   det->CLayerW.SetMother(det->world);
   dFORs(ilayer, 0, det->CLayerW.nlayers) {
     vol = det->CLayerW.DefaultChild();
     vol.name = det->CLayerW.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CLayerW.Add(vol);
   }
   
   // Al:
   thicknesses = ReadUv("C", "Al-thickness", nlayers + 1, 0.);
   alTypes     = ReadIv("C", "Al-type", nlayers + 1, 0);
   ywidth      = ReadU("C", "Al-y-width", 0.);
   zwidth      = ReadU("C", "Al-z-width", 0.);
   det->CSupportAl.nlayers = nlayers + 1;
   det->CSupportAl.name = "CSupportAl";
   det->CSupportAl.SetMother(det->world);
   dFORs(ilayer, 0, det->CSupportAl.nlayers) {
     vol = det->CSupportAl.DefaultChild();
     vol.name = det->CSupportAl.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CSupportAl.Add(vol);
   }
   
   // CFRP (Top and Bot):
   thicknesses = ReadUv("C", "CFRP-thickness", nlayers + 1, 0.);
   ywidth      = ReadU ("C", "CFRP-y-width", 0.);
   zwidth      = ReadU ("C", "CFRP-z-width", 0.);
   det->CSupportTopCFRP.nlayers = nlayers + 1;
   det->CSupportTopCFRP.name = "CSupportTopCFRP";
   det->CSupportTopCFRP.mater = det->materCFRP;
   det->CSupportTopCFRP.SetMother(det->world);
   det->CSupportBotCFRP = det->CSupportTopCFRP;
   det->CSupportBotCFRP.name = "CSupportBotCFRP";
   dFORs(ilayer, 0, det->CSupportTopCFRP.nlayers) {
     vol = det->CSupportTopCFRP.DefaultChild();
     vol.name = det->CSupportTopCFRP.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CSupportTopCFRP.Add(vol);
     vol = det->CSupportBotCFRP.DefaultChild();
     vol.name = det->CSupportBotCFRP.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CSupportBotCFRP.Add(vol);
   }
   
   // gap:
   G4double gap_C_S1 = ReadU("gaps", "C--S1", 0.);
   
   
   ///////////////////////////////////
  //              S1               //
 ///////////////////////////////////
   
   G4bool S1_supports = ReadB("S1", "supports", true);
   
   // common:
   det->S1.attr.scint.nstrips      = ReadI("S1", "n-strips", 1);
   det->S1.attr.scint.s_offset     = ReadU("S1", "strip-offset", 0.);
   det->S1.attr.scint.p_nbins      = ReadI("S1", "p-bins", 1);
   det->S1.attr.scint.t_nbins      = ReadI("S1", "t-bins", 1);
   det->S1.attr.scint.t_min        = ReadU("S1", "t-min", 0*ns);
   det->S1.attr.scint.t_max        = ReadU("S1", "t-max", 1*s);
   det->S1.attr.scint.speedoflight = ReadU("S1", "t-max", speed_of_light_in_vacuum);
   
   // scintillator:
   det->S1.name = "S1";
   thickness = ReadU("S1", "pSty-thickness", 0.);
   ywidth    = ReadU("S1", "pSty-y-width", 0.);
   zwidth    = ReadU("S1", "pSty-z-width", 0.);
   det->S1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S1.SetMother(det->world);
   det->S1.SetMater(det->materPSty);
   
   // Al:
   det->S1SupportAl.name = "S1SupportAl";
   thickness = ReadU("S1", "Al-thickness", 0.);
   ywidth    = ReadU("S1", "Al-y-width", 0.);
   zwidth    = ReadU("S1", "Al-z-width", 0.);
   alType    = ReadI("S1", "Al-type", 0);
   det->S1SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S1SupportAl.SetMother(det->world);
   det->S1SupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->S1SupportTopCFRP.name = "S1SupportTopCFRP";
   thickness = ReadU("S1", "CFRP-thickness", 0.);
   ywidth    = ReadU("S1", "CFRP-y-width", 0.);
   zwidth    = ReadU("S1", "CFRP-z-width", 0.);
   det->S1SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S1SupportTopCFRP.SetMother(det->world);
   det->S1SupportTopCFRP.SetMater(det->materCFRP);
   det->S1SupportBotCFRP = det->S1SupportTopCFRP;
   det->S1SupportBotCFRP.name = "S1SupportBotCFRP";
   
   G4double dist_S1_S2 = ReadU("gaps", "S1--S2");
   
   ///////////////////////////////////
  //              S2               //
 ///////////////////////////////////
   
   G4bool S2_supports = ReadB("S2", "supports", true);
   
   // common:
   det->S2.attr.scint.nstrips      = ReadI("S2", "n-strips", 1);
   det->S2.attr.scint.s_offset     = ReadU("S2", "strip-offset", 0.);
   det->S2.attr.scint.p_nbins      = ReadI("S2", "p-bins", 1);
   det->S2.attr.scint.t_nbins      = ReadI("S2", "t-bins", 1);
   det->S2.attr.scint.t_min        = ReadU("S2", "t-min", 0*ns);
   det->S2.attr.scint.t_max        = ReadU("S2", "t-max", 1*s);
   det->S2.attr.scint.speedoflight = ReadU("S2", "speed-of-light", speed_of_light_in_vacuum);
   
   // scintillator:
   det->S2.name = "S2";
   thickness = ReadU("S2", "pSty-thickness", 0.);
   ywidth    = ReadU("S2", "pSty-y-width", 0.);
   zwidth    = ReadU("S2", "pSty-z-width", 0.);
   det->S2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S2.SetMother(det->world);
   det->S2.SetMater(det->materPSty);
   
   // Al:
   det->S2SupportAl.name = "S2SupportAl";
   thickness = ReadU("S2", "Al-thickness", 0.);
   ywidth    = ReadU("S2", "Al-y-width", 0.);
   zwidth    = ReadU("S2", "Al-z-width", 0.);
   alType    = ReadI("S2", "Al-type", 0);
   det->S2SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S2SupportAl.SetMother(det->world);
   det->S2SupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->S2SupportTopCFRP.name = "S2SupportTopCFRP";
   thickness = ReadU("S2", "CFRP-thickness", 0.);
   ywidth    = ReadU("S2", "CFRP-y-width", 0.);
   zwidth    = ReadU("S2", "CFRP-z-width", 0.);
   det->S2SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S2SupportTopCFRP.SetMother(det->world);
   det->S2SupportTopCFRP.SetMater(det->materCFRP);
   det->S2SupportBotCFRP = det->S2SupportTopCFRP;
   det->S2SupportBotCFRP.name = "S2SupportBotCFRP";
   
   G4double gap_S2_CC1 = ReadU("gaps", "S2--CC1");
   
   
   ///////////////////////////////////
  //              CC1              //
 ///////////////////////////////////
   
   G4bool CC1_supports = ReadB("CC1", "supports", true);
   
   // common:
   nlayers     = ReadI("CC1", "n-layers", 1);
   pitchY      = ReadU("CC1", "y-pitch", cm);
   pitchZ      = ReadU("CC1", "z-pitch", cm);
   spreading   = ReadU("CC1", "spreading", -1.);
   CC1_gaps_CS = ReadUv("CC1", "gaps(CsI--Si)", nlayers, front(ReadUv("CC1", "gaps", nlayers, 0.)));
   CC1_gaps_SC = ReadUv("CC1", "gaps(Si--CsI)", nlayers-1, front(CC1_gaps_CS));
   
   // Si (Y and Z):
   thicknesses = ReadUv("CC1", "Si-thickness", nlayers, 0.);
   ywidth      = ReadU("CC1", "Si-y-width", 0.);
   zwidth      = ReadU("CC1", "Si-z-width", 0.);
   det->CC1LayerSiY.nlayers = nlayers;
   det->CC1LayerSiY.name = "CC1LayerSiY";
   det->CC1LayerSiY.mater = det->materSi;
   det->CC1LayerSiY.SetMother(det->world);
   det->CC1LayerSiY.attr.strip.spreading = spreading;
   det->CC1LayerSiY.attr.strip.nstrips = G4int(ywidth/pitchY);
   det->CC1LayerSiZ = det->CC1LayerSiY;
   det->CC1LayerSiZ.name = "CC1LayerSiZ";
   det->CC1LayerSiZ.attr.strip.nstrips = G4int(zwidth/pitchZ);
   dFORs(ilayer, 0, det->CC1LayerSiY.nlayers) {
     vol = det->CC1LayerSiY.DefaultChild();
     vol.name = det->CC1LayerSiY.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerSiY.Add(vol);
     vol = det->CC1LayerSiZ.DefaultChild();
     vol.name = det->CC1LayerSiZ.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerSiZ.Add(vol);
   }
   
   // CsI(Tl):
   G4double XoCsI = det->materCsI->GetRadlen();
   thicknesses = ReadUv("CC1", "CsI-thickness", nlayers, 0., XoCsI);
   ywidth      = ReadU("CC1", "CsI-y-width", 0., XoCsI);
   zwidth      = ReadU("CC1", "CsI-z-width", 0., XoCsI);
   det->CC1LayerCsI.nlayers = nlayers;
   det->CC1LayerCsI.name = "CC1LayerCsI";
   det->CC1LayerCsI.mater = det->materCsI;
   det->CC1LayerCsI.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1LayerCsI.nlayers) {
     vol = det->CC1LayerCsI.DefaultChild();
     vol.name = det->CC1LayerCsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1LayerCsI.Add(vol);
   }

   //det->CC1LayerSiY.Print();
   //det->CC1LayerSiZ.Print();
   //det->CC1LayerCsI.Print();
   //Disp(det->CC1LayerCsI[0].dims.x()/cm, "DEBUG      thickness = ");
   //G4cerr << "DEBUG      thickness = " << (det->CC1LayerCsI[0].dims.x()/cm) << "" << G4endl;
   
   // Al:
   thicknesses = ReadUv("CC1", "Al(CsI)-thickness", nlayers, front(ReadUv("CC1", "Al-thickness", nlayers+1, 0.)));
   alTypes     = ReadIv("CC1", "Al(CsI)-type", nlayers,      front(ReadIv("CC1", "Al-type", nlayers+1, 0)));
   ywidth      = ReadU("CC1", "Al(CsI)-y-width",             ReadU("CC1", "Al-y-width", 0.));
   zwidth      = ReadU("CC1", "Al(CsI)-z-width",             ReadU("CC1", "Al-z-width", 0.));
   det->CC1SupportAl_CsI.nlayers = nlayers;
   det->CC1SupportAl_CsI.name = "CC1SupportAl(CsI)";
   det->CC1SupportAl_CsI.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1SupportAl_CsI.nlayers) {
     vol = det->CC1SupportAl_CsI.DefaultChild();
     vol.name = det->CC1SupportAl_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CC1SupportAl_CsI.Add(vol);
   }
   thicknesses = ReadUv("CC1", "Al(Si)-thickness", nlayers, front(ReadUv("CC1", "Al-thickness", nlayers+1, 0.)));
   alTypes     = ReadIv("CC1", "Al(Si)-type", nlayers,      front(ReadIv("CC1", "Al-type", nlayers+1, 0)));
   ywidth      = ReadU("CC1", "Al(Si)-y-width",             ReadU("CC1", "Al-y-width", 0.));
   zwidth      = ReadU("CC1", "Al(Si)-z-width",             ReadU("CC1", "Al-z-width", 0.));
   det->CC1SupportAl_Si.nlayers = nlayers;
   det->CC1SupportAl_Si.name = "CC1SupportAl(Si)";
   det->CC1SupportAl_Si.SetMother(det->world);
   dFORs(ilayer, 0, det->CC1SupportAl_Si.nlayers) {
     vol = det->CC1SupportAl_Si.DefaultChild();
     vol.name = det->CC1SupportAl_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     vol.SetMater(AlType(alTypes[ilayer]));
     det->CC1SupportAl_Si.Add(vol);
   }
   
   // CFRP (Top and Bot):
   thicknesses = ReadUv("CC1", "CFRP(CsI)-thickness", nlayers, front(ReadUv("CC1", "CFRP-thickness", nlayers+1, 0.)));
   ywidth      = ReadU("CC1", "CFRP(CsI)-y-width",             ReadU("CC1", "CFRP-y-width", 0.));
   zwidth      = ReadU("CC1", "CFRP(CsI)-z-width",             ReadU("CC1", "CFRP-z-width", 0.));
   det->CC1SupportTopCFRP_CsI.nlayers = nlayers;
   det->CC1SupportTopCFRP_CsI.name = "CC1SupportTopCFRP(CsI)";
   det->CC1SupportTopCFRP_CsI.mater = det->materCFRP;
   det->CC1SupportTopCFRP_CsI.SetMother(det->world);
   det->CC1SupportBotCFRP_CsI = det->CC1SupportTopCFRP_CsI;
   det->CC1SupportBotCFRP_CsI.name = "CC1SupportBotCFRP(CsI)";
   dFORs(ilayer, 0, det->CC1SupportTopCFRP_CsI.nlayers) {
     vol = det->CC1SupportTopCFRP_CsI.DefaultChild();
     vol.name = det->CC1SupportTopCFRP_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportTopCFRP_CsI.Add(vol);
     vol = det->CC1SupportBotCFRP_CsI.DefaultChild();
     vol.name = det->CC1SupportBotCFRP_CsI.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportBotCFRP_CsI.Add(vol);
   }
   thicknesses = ReadUv("CC1", "CFRP(Si)-thickness", nlayers, front(ReadUv("CC1", "CFRP-thickness", nlayers+1, 0.)));
   ywidth      = ReadU("CC1", "CFRP(Si)-y-width",             ReadU("CC1", "CFRP-y-width", 0.));
   zwidth      = ReadU("CC1", "CFRP(Si)-z-width",             ReadU("CC1", "CFRP-z-width", 0.));
   det->CC1SupportTopCFRP_Si.nlayers = nlayers;
   det->CC1SupportTopCFRP_Si.name = "CC1SupportTopCFRP(Si)";
   det->CC1SupportTopCFRP_Si.mater = det->materCFRP;
   det->CC1SupportTopCFRP_Si.SetMother(det->world);
   det->CC1SupportBotCFRP_Si = det->CC1SupportTopCFRP_Si;
   det->CC1SupportBotCFRP_Si.name = "CC1SupportBotCFRP(Si)";
   dFORs(ilayer, 0, det->CC1SupportTopCFRP_Si.nlayers) {
     vol = det->CC1SupportTopCFRP_Si.DefaultChild();
     vol.name = det->CC1SupportTopCFRP_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportTopCFRP_Si.Add(vol);
     vol = det->CC1SupportBotCFRP_Si.DefaultChild();
     vol.name = det->CC1SupportBotCFRP_Si.name + "[" + TFormat::itoa(ilayer) + "]";
     vol.dims.set(thicknesses[ilayer]/2, ywidth/2, zwidth/2);
     det->CC1SupportBotCFRP_Si.Add(vol);
   }
   
   G4double dist_S2_S3 = ReadU("gaps", "S2--S3");
   
   
   ///////////////////////////////////
  //              S3               //
 ///////////////////////////////////
   
   G4bool S3_supports = ReadB("S3", "supports", true);
   
   // common:
   det->S3.attr.scint.nstrips      = ReadI("S3", "n-strips", 1);
   det->S3.attr.scint.s_offset     = ReadU("S3", "strip-offset", 0.);
   det->S3.attr.scint.p_nbins      = ReadI("S3", "p-bins", 1);
   det->S3.attr.scint.t_nbins      = ReadI("S3", "t-bins", 1);
   det->S3.attr.scint.t_min        = ReadU("S3", "t-min", 0*ns);
   det->S3.attr.scint.t_max        = ReadU("S3", "t-max", 1*s);
   det->S3.attr.scint.speedoflight = ReadU("S3", "speed-of-light", speed_of_light_in_vacuum);
   
   // scintillator:
   det->S3.name = "S3";
   thickness = ReadU("S3", "pSty-thickness", 0.);
   ywidth    = ReadU("S3", "pSty-y-width", 0.);
   zwidth    = ReadU("S3", "pSty-z-width", 0.);
   det->S3.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S3.SetMother(det->world);
   det->S3.SetMater(det->materPSty);
   
   // Al:
   det->S3SupportAl.name = "S3SupportAl";
   thickness = ReadU("S3", "Al-thickness", 0.);
   ywidth    = ReadU("S3", "Al-y-width", 0.);
   zwidth    = ReadU("S3", "Al-z-width", 0.);
   alType    = ReadI("S3", "Al-type", 0);
   det->S3SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S3SupportAl.SetMother(det->world);
   det->S3SupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->S3SupportTopCFRP.name = "S3SupportTopCFRP";
   thickness = ReadU("S3", "CFRP-thickness", 0.);
   ywidth    = ReadU("S3", "CFRP-y-width", 0.);
   zwidth    = ReadU("S3", "CFRP-z-width", 0.);
   det->S3SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S3SupportTopCFRP.SetMother(det->world);
   det->S3SupportTopCFRP.SetMater(det->materCFRP);
   det->S3SupportBotCFRP = det->S3SupportTopCFRP;
   det->S3SupportBotCFRP.name = "S3SupportBotCFRP";
   
   G4double gap_S3_CC2 = ReadU("gaps", "S3--CC2");
   
   
   ///////////////////////////////////
  //              CC2              //
 ///////////////////////////////////
   
   G4bool CC2_supports = ReadB("CC2", "supports");
   
   BEGIN_SCOPE
    
    det->CC2_is_segmented = CC2_segmented = ReadB("CC2", "segmented", true);
    nlayers = ReadI("CC2", "n-layers", 1);
    G4double CFRP_thickness = ReadU("CC2", "CFRP-between-thickness", 0.);
    G4double film_thickness = ReadU("CC2", "mirrorfilm-thickness", 0.);
    vector<G4double> item_height = ReadUv("CC2", "item-heights", nlayers, 0., det->materCsI->GetRadlen());
    vector<G4int> nitems_y = ReadIv("CC2", "n-items[Y]", nlayers, 1);
    vector<G4double> scint_width_y = ReadUv("CC2", "scint-width", nlayers, 0.);
    vector<G4double> CFRP_width_y = ReadUv("CC2", "CFRP-between-items-thickness[Y]", nlayers, 0.);
    vector<G4int> nitems_z = ReadIv("CC2", "n-items[Z]", nlayers, 1);
    vector<G4double>& scint_width_z = scint_width_y; // currently, y = z
    vector<G4double> CFRP_width_z = ReadUv("CC2", "CFRP-between-items-thickness[Z]", nlayers, 0.);
    
    G4double full_height; // physycal
    G4double wx = 0; // virtual
    vector<G4double> ywidths(nlayers), zwidths(nlayers); // physical
    vector<G4double> wy(nlayers), wz(nlayers); // virtual
    vector<G4double> dx(nlayers), dy(nlayers), dz(nlayers); // pitch
    
    dFORs(ilayer, 0, nlayers) {
      wy.at(ilayer) = nitems_y.at(ilayer) *  (( dy.at(ilayer) = scint_width_y.at(ilayer) + 2*film_thickness + CFRP_width_y.at(ilayer) + 4*microgap )) ;
      ywidths.at(ilayer) = wy.at(ilayer) + CFRP_width_y.at(ilayer);
      wz.at(ilayer) = nitems_z.at(ilayer) *  (( dz.at(ilayer) = scint_width_z.at(ilayer) + 2*film_thickness + CFRP_width_z.at(ilayer) + 4*microgap ));
      zwidths.at(ilayer) = wz.at(ilayer) + CFRP_width_z.at(ilayer);
      wx += dx.at(ilayer) = CFRP_thickness + 2*film_thickness + item_height.at(ilayer) + 4*microgap;
    }
    full_height = wx + CFRP_thickness;
    
      
      // scintillator:
      det->CC2_single.attr.calo3d.nx = nlayers;
      det->CC2_single.attr.calo3d.wx = wx;
      det->CC2_single.attr.calo3d.ny = nitems_y;//.assign(nlayers,0);
      det->CC2_single.attr.calo3d.wy = wy;//.assign(nlayers,0.);
      det->CC2_single.attr.calo3d.nz = nitems_z;//.assign(nlayers,0);
      det->CC2_single.attr.calo3d.wz = wz;//.assign(nlayers,0.);
    UNLESS( CC2_segmented ) // not segmented
      //dFORs(ilayer, 0, nlayers) {
      //  det->CC2.attr.calo3d.wy.at(ilayer) = (det->CC2.attr.calo3d.ny.at(ilayer) = nitems_y.at(ilayer)) * ywidths.at(ilayer);
      //  det->CC2.attr.calo3d.wz.at(ilayer) = (det->CC2.attr.calo3d.nz.at(ilayer) = nitems_z.at(ilayer)) * ywidths.at(ilayer);
      //}
      ywidth = ywidths.front(), zwidth = zwidths.front();
      det->CC2_single.dims.setX(full_height/2);
      det->CC2_single.dims.setY(ywidth/2);
      det->CC2_single.dims.setZ(zwidth/2);
      det->CC2_single.SetMother(det->world);
      det->CC2_single.SetMater(det->materCsI);
      
      // CFRP:
      det->CC2SupportTopCFRP.name = "CC2SupportTopCFRP";
      det->CC2SupportTopCFRP.dims.set(CFRP_thickness/2, ywidth/2, zwidth/2);
      det->CC2SupportTopCFRP.SetMother(det->world);
      det->CC2SupportTopCFRP.SetMater(det->materCFRP);
      
      det->CC2SupportBotCFRP.name = "CC2SupportBotCFRP";
      det->CC2SupportBotCFRP.dims.set(CFRP_thickness/2, ywidth/2, zwidth/2);
      det->CC2SupportBotCFRP.SetMother(det->world);
      det->CC2SupportBotCFRP.SetMater(det->materCFRP);
      
    ELSE // segmented
      
      #include "subfiles/Geometry.CC2.icc"
      
    END_IF
    
   END_SCOPE
   
   G4double gap_CC2_Plate = ReadU("gaps", "CC2--plate", 0.);
   
   
   ///////////////////////////////////
  //           AlPlate             //
 ///////////////////////////////////
   
   det->CC2SupportTopAl_o.name = "CC2SupportTopAl_o";
   thickness = ReadU("plate", "upper-outer-thickness", 0.);
   ywidth    = ReadU("plate", "y-width", 0.);
   zwidth    = ReadU("plate", "z-width", 0.);
   det->CC2SupportTopAl_o.SetMother(det->world);
   det->CC2SupportTopAl_o.SetMater(det->materAl);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportTopAl_o.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportTopAl_o.dims.set(thickness/2, ywidth/2, zwidth/2);
   
   det->CC2SupportTopAl_i.name = "CC2SupportTopAl_i";
   thickness = ReadU("plate", "upper-inner-thickness", 0.);
   det->CC2SupportTopAl_i.SetMother(det->world);
   det->CC2SupportTopAl_i.SetMater(det->materAl108);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportTopAl_i.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportTopAl_i.dims.set(thickness/2, ywidth/2, zwidth/2);
   
   det->CC2SupportBotAl_i.name = "CC2SupportBotAl_i";
   thickness = ReadU("plate", "lower-inner-thickness", 0.);
   det->CC2SupportBotAl_i.SetMother(det->world);
   det->CC2SupportBotAl_i.SetMater(det->materAl108);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportBotAl_i.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportBotAl_i.dims.set(thickness/2, ywidth/2, zwidth/2);
   
   det->CC2SupportBotAl_o.name = "CC2SupportBotAl_o";
   thickness = ReadU("plate", "lower-outer-thickness", ReadU("plate", "thickness", 0.));
   det->CC2SupportBotAl_o.SetMother(det->world);
   det->CC2SupportBotAl_o.SetMater(det->materAl);
   IF_ thickness < 0 THEN
     thickness = -thickness;
     det->CC2SupportBotAl_o.SetMater(det->materVacuum);
   END_IF
   det->CC2SupportBotAl_o.dims.set(thickness/2, ywidth/2, zwidth/2);
   
   G4double gap_Plate_S4 = ReadU("gaps", "CC2--S4", ReadU("gaps", "plate--S4", 0.));
   
   ///////////////////////////////////
  //              S4               //
 ///////////////////////////////////
   
   G4bool S4_supports = ReadB("S4", "supports", true);
   
   // common:
   det->S4.attr.scint.nstrips      = ReadI("S4", "n-strips", 1);
   det->S4.attr.scint.s_offset     = ReadU("S4", "strip-offset", 0.);
   det->S4.attr.scint.p_nbins      = ReadI("S4", "p-bins", 1);
   det->S4.attr.scint.t_nbins      = ReadI("S4", "t-bins", 1);
   det->S4.attr.scint.t_min        = ReadU("S4", "t-min", 0*ns);
   det->S4.attr.scint.t_max        = ReadU("S4", "t-max", 1*s);
   det->S4.attr.scint.speedoflight = ReadU("S4", "speed-of-light", speed_of_light_in_vacuum);
   
   // scintillator:
   det->S4.name = "S4";
   thickness = ReadU("S4", "pSty-thickness", 0.);
   ywidth    = ReadU("S4", "pSty-y-width", 0.);
   zwidth    = ReadU("S4", "pSty-z-width", 0.);
   det->S4.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S4.SetMother(det->world);
   det->S4.SetMater(det->materPSty);
   
   // Al:
   det->S4SupportAl.name = "S4SupportAl";
   thickness = ReadU("S4", "Al-thickness", 0.);
   ywidth    = ReadU("S4", "Al-y-width", 0.);
   zwidth    = ReadU("S4", "Al-z-width", 0.);
   alType    = ReadI("S4", "Al-type", 0);
   det->S4SupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S4SupportAl.SetMother(det->world);
   det->S4SupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->S4SupportTopCFRP.name = "S4SupportTopCFRP";
   thickness = ReadU("S4", "CFRP-thickness", 0.);
   ywidth    = ReadU("S4", "CFRP-y-width", 0.);
   zwidth    = ReadU("S4", "CFRP-z-width", 0.);
   det->S4SupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->S4SupportTopCFRP.SetMother(det->world);
   det->S4SupportTopCFRP.SetMater(det->materCFRP);
   det->S4SupportBotCFRP = det->S4SupportTopCFRP;
   det->S4SupportBotCFRP.name = "S4SupportBotCFRP";
   
   G4double gap_S4_ND = ReadU("gaps", "S4--ND", 0.);
   
   ///////////////////////////////////
  //              ND               //
 ///////////////////////////////////
   
   G4bool ND_supports = ReadB("ND", "supports", true);
   
   // pEth:
   det->ND_pEth1.name = "ND_pEth1";
   thickness = ReadU("ND", "pEth1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth1.SetMother(det->world);
   det->ND_pEth1.SetMater(det->materPEth);
   
   det->ND_pEth2.name = "ND_pEth2";
   thickness = ReadU("ND", "pEth2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth2.SetMother(det->world);
   det->ND_pEth2.SetMater(det->materPEth);
   
   det->ND_pEth3.name = "ND_pEth3";
   thickness = ReadU("ND", "pEth3-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_pEth3.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_pEth3.SetMother(det->world);
   det->ND_pEth3.SetMater(det->materPEth);
   
   // PMMA:
   det->ND_PMMA1.name = "ND_PMMA1";
   thickness = ReadU("ND", "PMMA1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA1.SetMother(det->world);
   det->ND_PMMA1.SetMater(det->materPMMA);
   
   det->ND_PMMA2.name = "ND_PMMA2";
   thickness = ReadU("ND", "PMMA2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA2.SetMother(det->world);
   det->ND_PMMA2.SetMater(det->materPMMA);
   
   det->ND_PMMA3.name = "ND_PMMA3";
   thickness = ReadU("ND", "PMMA3-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA3.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA3.SetMother(det->world);
   det->ND_PMMA3.SetMater(det->materPMMA);
   
   det->ND_PMMA4.name = "ND_PMMA4";
   thickness = ReadU("ND", "PMMA4-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_PMMA4.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_PMMA4.SetMother(det->world);
   det->ND_PMMA4.SetMater(det->materPMMA);
   
   // LiF ZnS:
   det->ND_LiFZnS1.name = "ND_LiFZnS1";
   thickness = ReadU("ND", "LiFZnS1-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_LiFZnS1.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_LiFZnS1.SetMother(det->world);
   det->ND_LiFZnS1.SetMater(det->materLiFZnS);
   
   det->ND_LiFZnS2.name = "ND_LiFZnS2";
   thickness = ReadU("ND", "LiFZnS2-thickness", 0.);
   ywidth    = ReadU("ND", "y-width", 0.);
   zwidth    = ReadU("ND", "z-width", 0.);
   det->ND_LiFZnS2.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->ND_LiFZnS2.SetMother(det->world);
   det->ND_LiFZnS2.SetMater(det->materLiFZnS);
   
   // Al:
   det->NDSupportAl.name = "NDSupportAl";
   thickness = ReadU("ND", "Al-thickness", 0.);
   ywidth    = ReadU("ND", "Al-y-width", 0.);
   zwidth    = ReadU("ND", "Al-z-width", 0.);
   alType    = ReadI("ND", "Al-type", 0);
   det->NDSupportAl.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->NDSupportAl.SetMother(det->world);
   det->NDSupportAl.SetMater(AlType(alType));
   
   // CFRP:
   det->NDSupportTopCFRP.name = "NDSupportTopCFRP";
   thickness = ReadU("ND", "CFRP-thickness", 0.);
   ywidth    = ReadU("ND", "CFRP-y-width", 0.);
   zwidth    = ReadU("ND", "CFRP-z-width", 0.);
   det->NDSupportTopCFRP.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->NDSupportTopCFRP.SetMother(det->world);
   det->NDSupportTopCFRP.SetMater(det->materCFRP);
   det->NDSupportBotCFRP = det->NDSupportTopCFRP;
   det->NDSupportBotCFRP.name = "NDSupportBotCFRP";
   
   G4double gap_ND_BottomStuff = ReadU("gaps", "ND--bottom-stuff", 0.);
   
   
   ///////////////////////////////////
  //         Bottom Stuff          //
 ///////////////////////////////////
   
   G4bool BottomStuff_supports = ReadB("Bottom Stuff", "supports", true);
   
   det->BottomStuff.name = "BottomStuff";
   thickness = ReadU("Bottom Stuff", "x-size", 0.);
   ywidth    = ReadU("Bottom Stuff", "y-size", 0.);
   zwidth    = ReadU("Bottom Stuff", "z-size", 0.);
   det->BottomStuff.dims.set(thickness/2, ywidth/2, zwidth/2);
   det->BottomStuff.SetMother(det->world);
   det->BottomStuff.SetMater(det->materBottomStuff);
   
   
   ///////////////////////////////////
  //             Gaps              //
 ///////////////////////////////////
   
   #define THICK(d) ( det->d.dims.x()*2 )
   #define TOP_PLANE(d) (det->d.pos.x() - det->d.dims.x())
   #define BOT_PLANE(d) (det->d.pos.x() + det->d.dims.x())
   /*G4double dist_S1_SiArray2 = dist_S1_S2 - gap_SiArray2_S2 - THICK(SiArray2)
      - THICK(SiArray2SupportTopCFRP) - THICK(SiArray2SupportBotCFRP) - THICK(SiArray2SupportAl) - .5*THICK(S2);
   G4double gap_S1_SiArray2 = dist_S1_SiArray2 - THICK(S1SupportTopCFRP) - THICK(S1SupportBotCFRP)
      - THICK(S1SupportAl) - .5*THICK(S1);*/
   G4double gap_S1_S2 = dist_S1_S2 - .5*THICK(S1) - .5*THICK(S2) - THICK(S1SupportAl) - 2*THICK(S1SupportBotCFRP);
   // G4double gap_CC1_S3 = dist_S2_S3 - THICK(S2SupportTopCFRP) - THICK(S2SupportBotCFRP) - THICK(S2SupportAl) - gap_S2_CC1
   //    - .5*THICK(S2) - .5*THICK(S3);
   // if (!det->CC1LayerSiZ.empty())
   //   gap_CC1_S3 -= BOT_PLANE(CC1LayerSiZ.back()) -
   //                 TOP_PLANE(CC1SupportTopCFRP_CsI.front());

   
   ///////////////////////////////////
  //       Lateral detectors       //
 ///////////////////////////////////
   
   G4double xwidth;
   enum EDetSide {
     kLeft  = DetectorConstruction::kLeft,  /* -z */
     kFront = DetectorConstruction::kFront, /* -y */
     kRight = DetectorConstruction::kRight, /* +z */
     kBack  = DetectorConstruction::kBack   /* +y */
   };
   
   G4double sideAC_outerdist;
   G4double gap_topAC_sideAC = ReadU("gaps", "topAC--sideAC", 0.);
   xwidth    = ReadU("sideAC", "length", 0.);
   thickness = ReadU("sideAC", "thickness", 0.);
   sideAC_outerdist = ReadU("gaps", "topAC<>sideAC", 0) + det->topAC.dims.y() + thickness;
   det->sideAC_horizontal = ReadB("sideAC", "strips-horizintal", false);
   det->sideAC.attr.scint.nstrips      = ReadI("sideAC", "n-strips", 1);
   det->sideAC.attr.scint.s_offset     = ReadU("sideAC", "strip-offset", 0.);
   det->sideAC.attr.scint.p_nbins      = ReadI("sideAC", "p-bins", 1);
   det->sideAC.attr.scint.t_nbins      = ReadI("sideAC", "t-bins", 1);
   det->sideAC.attr.scint.t_min        = ReadU("sideAC", "t-min", 0*ns);
   det->sideAC.attr.scint.t_max        = ReadU("sideAC", "t-max", 1*s);
   det->sideAC.attr.scint.speedoflight = ReadU("sideAC", "speed-of-light", speed_of_light_in_vacuum);
   
   
   det->sideAC.name = "sideAC";
   det->sideAC.SetMater(det->materPSty);
   det->sideAC.SetMother(det->world);
   det->sideAC.assign(4, det->sideAC.DefaultChild());
   det->sideAC[kLeft].dims.set(.5*xwidth, .5*thickness, sideAC_outerdist + microgap - .5*thickness);
   det->sideAC[kRight].dims = det->sideAC[kLeft].dims;
   det->sideAC[kBack].dims.set(.5*xwidth, sideAC_outerdist + microgap - .5*thickness, .5*thickness);
   det->sideAC[kFront].dims = det->sideAC[kBack].dims;
   det->sideAC[kLeft].name  += "[left]";
   det->sideAC[kRight].name += "[right]";
   det->sideAC[kBack].name  += "[back]";
   det->sideAC[kFront].name += "[front]";
   
   
   G4double TD_outerdist;
   G4double offset_S2_TD = ReadU("gaps", "S2--TD", 0.);
   xwidth    = ReadU("TD", "length", 0.);
   thickness = ReadU("TD", "thickness", 0.);
   TD_outerdist = ReadU("gaps", "S2<>TD", 0.) + det->S2.dims.y() + thickness;
   det->TD.name = "TD";
   det->TD.SetMater(det->materPSty);
   det->TD.SetMother(det->world);
   det->TD.assign(4, det->TD.DefaultChild());
   det->TD[kLeft].dims.set(.5*xwidth, .5*thickness, TD_outerdist + microgap - .5*thickness);
   det->TD[kRight].dims = det->TD[kLeft].dims;
   det->TD[kBack].dims.set(.5*xwidth, TD_outerdist + microgap - .5*thickness, .5*thickness);
   det->TD[kFront].dims = det->TD[kBack].dims;
   det->TD[kLeft].name  += "[left]";
   det->TD[kRight].name += "[right]";
   det->TD[kBack].name  += "[back]";
   det->TD[kFront].name += "[front]";
   
   
   ///////////////////////////////////
  //           Build-up            //
 ///////////////////////////////////
   
   G4double currentpos = 0, tmppos;
   
   // World:
   det->world.CreateVolume();
   
   // topAC:
   det->topACSupportTopCFRP.CreateVolume(currentpos);
   det->topACSupportAl.CreateVolume(currentpos);
   det->topACSupportBotCFRP.CreateVolume(currentpos);
   det->topAC.CreateVolume(currentpos, gap_topAC_C);
   
   // sideAC:
   tmppos = currentpos - gap_topAC_C + gap_topAC_sideAC + det->sideAC.front().dims.x();
   det->sideAC[kLeft].pos.set(
     /* x */ tmppos,
     /* y */ - det->sideAC[kLeft].dims.z() - microgap,
     /* z */ - sideAC_outerdist - microgap + det->sideAC[kLeft].dims.z() );
   det->sideAC[kRight].pos.set(
     /* x */ tmppos,
     /* y */ + det->sideAC[kRight].dims.z() + microgap,
     /* z */ + sideAC_outerdist + microgap - det->sideAC[kRight].dims.z() );
   det->sideAC[kFront].pos.set(
     /* x */ tmppos,
     /* y */ - sideAC_outerdist - microgap+det->sideAC[kFront].dims.y(),
     /* z */ + det->sideAC[kFront].dims.y() + microgap );
   det->sideAC[kBack].pos.set(
     /* x */ tmppos,
     /* y */ + sideAC_outerdist+microgap - det->sideAC[kBack].dims.y(),
     /* z */ - det->sideAC[kBack].dims.y() - microgap );
   vFOR(BoxVolume, det->sideAC, subvol) subvol->CreateVolume();
   
   // C:
   bool C_interleave = ReadB("C", "interleave-layers", false);
   odd = true;
   nlayers = det->CLayerSiY.nlayers;
   if(nlayers) dFORs(ilayer, 0, nlayers+1) {
     det->CSupportTopCFRP[ilayer].CreateVolume(currentpos);
     det->CSupportAl     [ilayer].CreateVolume(currentpos);
     det->CSupportBotCFRP[ilayer].CreateVolume(currentpos);
     if(ilayer == nlayers) break;
     det->CLayerW        [ilayer].CreateVolume(currentpos);
     (odd?det->CLayerSiY:det->CLayerSiZ)[ilayer].CreateVolume(currentpos, C_gaps[ilayer]);
     (odd?det->CLayerSiZ:det->CLayerSiY)[ilayer].CreateVolume(currentpos);
     if(C_interleave) odd -= 1;                                                                                                                        };
   currentpos += gap_C_S1;
   
   // S1:
   det->S1.CreateVolume(currentpos);
   det->S1SupportTopCFRP.CreateVolume(currentpos);
   det->S1SupportAl.CreateVolume(currentpos);
   det->S1SupportBotCFRP.CreateVolume(currentpos, gap_S1_S2);
   
    // TD:
   tmppos = currentpos + offset_S2_TD;
   det->TD[kLeft] .pos.set(tmppos, -det->TD[kLeft] .dims.z()-microgap, -TD_outerdist-microgap+det->TD[kLeft] .dims.z());
   det->TD[kRight].pos.set(tmppos, +det->TD[kRight].dims.z()+microgap, +TD_outerdist+microgap-det->TD[kRight].dims.z());
   det->TD[kFront].pos.set(tmppos, -TD_outerdist-microgap+det->TD[kFront].dims.y(), +det->TD[kFront].dims.y()+microgap);
   det->TD[kBack] .pos.set(tmppos, +TD_outerdist+microgap-det->TD[kBack] .dims.y(), -det->TD[kBack] .dims.y()-microgap);
   vFOR(BoxVolume, det->TD, subvol) subvol->CreateVolume();
   
   currentpos = det->S1.pos.x() + dist_S1_S2 - det->S2.dims.x();
   
   // S2:
   det->S2.CreateVolume(currentpos);
   det->S2SupportTopCFRP.CreateVolume(currentpos);
   det->S2SupportAl.CreateVolume(currentpos);
   det->S2SupportBotCFRP.CreateVolume(currentpos, gap_S2_CC1);
   
   // CC1:
   //odd = true;
   nlayers = det->CC1LayerSiY.nlayers;
   if(nlayers) dFORs(ilayer, 0, nlayers) {
     det->CC1SupportTopCFRP_CsI[ilayer].CreateVolume(currentpos);
     det->CC1SupportAl_CsI     [ilayer].CreateVolume(currentpos);
     det->CC1SupportBotCFRP_CsI[ilayer].CreateVolume(currentpos);
     det->CC1LayerCsI          [ilayer].CreateVolume(currentpos, CC1_gaps_CS[ilayer]);
     det->CC1LayerSiY          [ilayer].CreateVolume(currentpos);
     det->CC1SupportTopCFRP_Si [ilayer].CreateVolume(currentpos);
     det->CC1SupportAl_Si      [ilayer].CreateVolume(currentpos);
     det->CC1SupportBotCFRP_Si [ilayer].CreateVolume(currentpos);
     det->CC1LayerSiZ          [ilayer].CreateVolume(currentpos);
     if(ilayer == nlayers) break;
     currentpos += CC1_gaps_SC[ilayer];
   };
  
   // currentpos += gap_CC1_S3;
   currentpos = det->S2.pos.x() + dist_S2_S3 - det->S3.dims.x();


   // S3:
   det->S3.CreateVolume(currentpos);
   det->S3SupportTopCFRP.CreateVolume(currentpos);
   det->S3SupportAl.CreateVolume(currentpos);
   det->S3SupportBotCFRP.CreateVolume(currentpos, gap_S3_CC2);
   
   // CC2:
   det->CC2SupportTopAl_o.CreateVolume(currentpos);
   det->CC2SupportTopAl_i.CreateVolume(currentpos);
   IF(CC2_segmented)
     //det->CC2_mirrorfilm.CreateVolume(currentpos, -2*det->CC2_mirrorfilm.dims.x(), true);
     det->CC2_CFRP_x.CreateVolume(currentpos, -2*det->CC2_CFRP_x.dims.x(), true);
     det->CC2_CFRP_y.CreateVolume(currentpos, -2*det->CC2_CFRP_y.dims.x(), true);
     det->CC2_CFRP_z.CreateVolume(currentpos, -2*det->CC2_CFRP_z.dims.x(), true);
     det->CC2.CreateVolume(currentpos, gap_CC2_Plate);
     //currentpos += 2*det->CC2.dims.x();
   ELSE
     det->CC2SupportTopCFRP.CreateVolume(currentpos);
     det->CC2_single.CreateVolume(currentpos);
     det->CC2SupportBotCFRP.CreateVolume(currentpos, gap_CC2_Plate);
   END_IF
   det->CC2SupportBotAl_i.CreateVolume(currentpos);
   det->CC2SupportBotAl_o.CreateVolume(currentpos, gap_Plate_S4);
   
   // Al Plate:
   //det->AlPlate.CreateVolume(currentpos, gap_Plate_S4);
   
   // det->CC2SupportTopAl_o.Print();
   // det->CC2SupportTopAl_i.Print();
   // det->CC2SupportBotAl_i.Print();
   // det->CC2SupportBotAl_o.Print();
   
   // S4:
   det->S4.CreateVolume(currentpos);
   det->S4SupportTopCFRP.CreateVolume(currentpos);
   det->S4SupportAl.CreateVolume(currentpos);
   det->S4SupportBotCFRP.CreateVolume(currentpos, gap_S4_ND);
   
   // ND:
   det->ND_pEth1.CreateVolume(currentpos);
     det->ND_PMMA1.CreateVolume(currentpos);
       det->ND_LiFZnS1.CreateVolume(currentpos);
     det->ND_PMMA2.CreateVolume(currentpos);
   det->ND_pEth2.CreateVolume(currentpos);
     det->ND_PMMA3.CreateVolume(currentpos);
       det->ND_LiFZnS2.CreateVolume(currentpos);
     det->ND_PMMA4.CreateVolume(currentpos);
   det->ND_pEth3.CreateVolume(currentpos);
   det->NDSupportTopCFRP.CreateVolume(currentpos);
   det->NDSupportAl.CreateVolume(currentpos);
   det->NDSupportBotCFRP.CreateVolume(currentpos, gap_ND_BottomStuff);
   
   // BottomStuff:
   det->BottomStuff.CreateVolume(currentpos);
   
   
   ///////////////////////////////////
  //           Supports            //
 ///////////////////////////////////
   
   unless( topAC_supports && global_supports )
     det->topACSupportTopCFRP =
     det->topACSupportAl      =
     det->topACSupportBotCFRP =
     det->materVacuum;
   unless( C_supports && global_supports )
     det->CSupportTopCFRP =
     det->CSupportBotCFRP =
     det->CSupportAl      =
     det->materVacuum;
   unless( S1_supports && global_supports )
     det->S1SupportTopCFRP =
     det->S1SupportAl      =
     det->S1SupportBotCFRP =
     det->materVacuum;
   unless( S2_supports && global_supports )
     det->S2SupportTopCFRP =
     det->S2SupportAl      =
     det->S2SupportBotCFRP =
     det->materVacuum;
   unless( CC1_supports && global_supports )
     det->CC1SupportTopCFRP_CsI =
     det->CC1SupportBotCFRP_CsI =
     det->CC1SupportAl_CsI =
     det->CC1SupportTopCFRP_Si =
     det->CC1SupportBotCFRP_Si =
     det->CC1SupportAl_Si =
     det->materVacuum;
   unless( S3_supports && global_supports )
     det->S3SupportTopCFRP =
     det->S3SupportAl      =
     det->S3SupportBotCFRP =
     det->materVacuum;
   unless( CC2_supports && global_supports )
     det->CC2SupportTopCFRP =
     det->CC2SupportBotCFRP =
     det->materVacuum;
   unless( S4_supports && global_supports )
     det->S4SupportTopCFRP =
     det->S4SupportAl      =
     det->S4SupportBotCFRP =
     det->materVacuum;
   unless( ND_supports && global_supports )
     det->NDSupportTopCFRP =
     det->NDSupportAl      =
     det->NDSupportBotCFRP =
     det->materVacuum;
   unless( BottomStuff_supports && global_supports )
     det->BottomStuff =
     det->materVacuum;
   
   return true;
   
END(Geometry::Read)
