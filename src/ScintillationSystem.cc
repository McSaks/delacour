#include "G4UnitsTable.hh"

#include "ScintillationSystem.hh"
#include "SensitiveScintStrip.hh"
#include "EventAction.hh"
#include "EventData.hh"
#include "Alternative.hh"
#include "FOR.hh"


ScintStripIndex ScintStripLayer::FindIndex(const G4ThreeVector& hitpos, G4double time) const
{
  G4double s_pos, p_pos;
  if(proj == kYStripProj)
    s_pos = hitpos.y(), p_pos = hitpos.z();
  else
    s_pos = hitpos.z(), p_pos = hitpos.y();
  G4int ss = findbin(s_min, s_bin(), s_pos);
  G4int pp = findbin(p_min, p_bin(), p_pos);
  G4int tt = findbin(t_min, t_bin(), time);
  return ScintStripIndex(ss, pp, tt);
}

void ScintStripLayer::Initialize()
{
  //t_nbins = p_nbins = s_nbins = 3;
  vector<ScintStripPoint> t_default           ( t_nbins, 0         );
  vector< vector<ScintStripPoint> > p_default ( p_nbins, t_default );
  assign  /*   *   *   *   *   *   *   *   */ ( s_nbins, p_default );
}


ScintStripSignals ScintStripLayer::GetSignal()
{
  ScintStripSignals sig;
  //sig.one.layer = sig.two.layer = this;
  sig.one.t_bin = sig.two.t_bin = t_bin();
  sig.one.t_min = sig.two.t_min = t_min;
  sig.one.s_min = sig.two.s_min = s_min;
  sig.one.s_max = sig.two.s_max = s_max;
  sig.one.s_nbins = sig.two.s_nbins = s_nbins;
  G4double dt = (p_max - p_min) / speedoflight;
  G4int tbins = t_nbins + (G4int)ceil( dt / t_bin() );
  vector<ScintStripPoint> t_default           ( tbins, 0 );
  sig.one.assign( s_nbins, t_default );
  sig.two.assign( s_nbins, t_default );
  
  G4double one_time, two_time; // when light arrives to the edge of crystal
  FORs(i_s, 0, s_nbins) {
  //vFORc(vector< vector<ScintStripPoint> >, *this, strip) {
    // (*this)[i_s][i_p][i_t] --> sig.###[i_s][j_t]
    FORs(i_p, 0, p_nbins) FORs(i_t, 0, t_nbins) {
      two_time = bincenter(t_min, t_bin(), i_t);
      one_time = two_time + bincenter(0, p_bin(), i_p)/speedoflight;
      two_time += (p_max - bincenter(p_min, p_bin(), i_p))/speedoflight;
      sig.one[i_s][ findbin(t_min, t_bin(), one_time) ] += operator()(i_s, i_p, i_t);
      sig.two[i_s][ findbin(t_min, t_bin(), two_time) ] += operator()(i_s, i_p, i_t);
    }
  }
  
  return sig;
}


G4double ScintStripSignal::GetActuationTime() const
{
  G4int b = -1;
  FORs(i_s, 0, s_nbins) {
    FORs(i_t, 0, (G4int)at(i_s).size()) {
      if( at(i_s).at(i_t).edep > 0.00 * MeV ) { // threshold
        if( b == -1  ||  i_t < b ) b = i_t;
        break;
      }
    }
  }
  if (b == -1) return SensitiveScintStrip::kNoSignal;
  return bincenter(t_min  -  0.5 * t_bin, t_bin, b);
}

G4double ScintStripSignal::GetActuationTime(G4int strip) const
{
  G4int b = -1;
  FORs(i_t, 0, (G4int)at(strip).size()) {
    if( at(strip).at(i_t).edep > 0.00 * MeV ) { // threshold
      if( b == -1  ||  i_t < b ) b = i_t;
      break;
    }
  }
  if (b == -1) return SensitiveScintStrip::kNoSignal;
  return bincenter(t_min  -  0.5 * t_bin, t_bin, b);
}


void ScintillationSystem::Clear()
{
  /*size_t s; Disp("__________________");
  Disp(s = layers.size(),"size: ");
  Disp(s? s=layers.front()->size():s,".size: ");
  Disp(s? layers.front()->front().size():s,"..size: ");
  Disp(s? layers.front()->front().front().size():s,"...size: ");*/
  vFOR(ScintStripLayer*, layers, l) {
    ScintStripLayerFOR(**l, pt)
      pt->edep = 0.0;
    (*l)->sensitive->earliest = SensitiveScintStrip::kNoSignal;
    (*l)->sensitive->latest   = SensitiveScintStrip::kNoSignal;
    (*l)->sensitive->Etot = 0.;
    std::vector<G4double>*& stripEtot =
      (*l)->sensitive->stripEtot;
    stripEtot->assign(stripEtot->size(), 1.);
    (*l)->sensitive->hassignal = false;
  }
}



G4bool ScintillationSystem::Write() const
{
  
  if(!data->ok()) return false;
  
  return true;
}


G4bool ToFSystem::Write() const
{
  ensure( data->ok() );
  ensure( ScintillationSystem::Write() );
  data->timeData.S1.data = S1;
  data->timeData.S2.data = S2;
  data->timeData.S3.data = S3;
  data->timeData.S4.data = S4;
  return true;
}

G4bool ACSystem::Write() const
{
  ensure( data->ok() );
  ensure( ScintillationSystem::Write() );
  data->timeData.topAC.data = topAC;
  data->timeData.sideAC_left.data = sideAC_left;
  data->timeData.sideAC_front.data = sideAC_front;
  data->timeData.sideAC_right.data = sideAC_right;
  data->timeData.sideAC_back.data = sideAC_back;
  return true;
}



G4bool ToFSystem::Set()
{
  vFOR(ScintStripLayer*, layers, l) {
    if(false) RELAX
    else if((*l)->sensitive->GetName() == "S1")
      S1 = *l, (data ? data->timeData.S1.data = S1 : NULL);
    else if((*l)->sensitive->GetName() == "S2")
      S2 = *l, (data ? data->timeData.S2.data = S2 : NULL);
    else if((*l)->sensitive->GetName() == "S3")
      S3 = *l, (data ? data->timeData.S3.data = S3 : NULL);
    else if((*l)->sensitive->GetName() == "S4")
      S4 = *l, (data ? data->timeData.S4.data = S4 : NULL);
  };
  if(S1 && S2 && S3 && S4) return true;
  return false;
}

G4bool ACSystem::Set()
{
  vFOR(ScintStripLayer*, layers, l) {
    if(false) RELAX
    else if((*l)->sensitive->GetName() == "topAC")
      topAC = *l, (data ? data->timeData.topAC.data = topAC : NULL);
    else if((*l)->sensitive->GetName() == "sideAC[left]")
      sideAC_left = *l, (data ? data->timeData.sideAC_left.data = sideAC_left : NULL);
    else if((*l)->sensitive->GetName() == "sideAC[front]")
      sideAC_front = *l, (data ? data->timeData.sideAC_left.data = sideAC_front : NULL);
    else if((*l)->sensitive->GetName() == "sideAC[right]")
      sideAC_right = *l, (data ? data->timeData.sideAC_left.data = sideAC_right : NULL);
    else if((*l)->sensitive->GetName() == "sideAC[back]")
      sideAC_back = *l, (data ? data->timeData.sideAC_left.data = sideAC_back : NULL);
  };
  if(topAC && sideAC_left && sideAC_front && sideAC_right && sideAC_back) return true;
  return false;
}




G4bool ACSystem::Trigger() const
{
  return true;
}


G4bool ToFSystem::Trigger(G4bool timeSelection) const
{
 IF( 0 ) // histogram-based
  G4double S1_earliest,         S2_earliest;
  G4int    S1_earliestbin,      S2_earliestbin;
  G4bool   S1_actuated = false, S2_actuated = false;
  G4double e;
  
  FORs(ss, 0, S1->s_nbins)
  FORs(pp, 0, S1->p_nbins)
  FORs(tt, 0, S1->t_nbins) {
    e = (*S1)(ss,pp,tt).edep;
    IF_ e > 0 THEN
      IF_ S1_actuated THEN
        { if(tt < S1_earliestbin) S1_earliestbin = tt; }
      ELSE
        S1_actuated = true, S1_earliestbin = tt;
      FI
    FI
  }
  ensure(S1_actuated);
  S1_earliest = binmax(S1->t_min, S1->t_bin(), S1_earliestbin);
  
  FORs(ss, 0, S2->s_nbins)
  FORs(pp, 0, S2->p_nbins)
  FORs(tt, 0, S2->t_nbins) {
    e = (*S2)(ss,pp,tt).edep;
    IF_ e > 0 THEN
      IF_ S2_actuated THEN
        { if(tt < S2_earliestbin) S2_earliestbin = tt; }
      ELSE
        S2_actuated = true, S2_earliestbin = tt;
      FI
    FI
  }
  ensure(S2_actuated);
  S2_earliest = binmin(S2->t_min, S2->t_bin(), S2_earliestbin);
  
  return timeSelection ? S1_earliest <= S2_earliest + 0.0001*ns : true;
 FI
 IF( 1 ) // absolute time
  ensure( S1->sensitive->HasSignal() );
  ensure( S2->sensitive->HasSignal() );
  return timeSelection ? S1->sensitive->GetEarliest() < S2->sensitive->GetEarliest() : true ;
 FI
 IF( 0 ) // signal-based
  return true;
 FI
 return true;
}
