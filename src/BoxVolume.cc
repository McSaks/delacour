#include "BoxVolume.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Material.hh"
#include "G4VSensitiveDetector.hh"
#include "G4PVPlacement.hh"
#include "FOR.hh"

BoxVolume::BoxVolume(const G4String& name_, BoxVolume& mommy)
  : name(name_), mother(&mommy)
  , solid(0), logic(0), physic(0), mater(0), sensitive(0)
{
  
}

BoxVolume::BoxVolume()
  : name("unnamed"), mother(NULL)
  , solid(0), logic(0), physic(0), mater(0), sensitive(0)
{
  
}

BoxVolume::~BoxVolume()
{
  delete0( solid     );
  delete0( logic     );
  delete0( physic    );
  //delete0( mater     );
  //delete0( sensitive );
}

void BoxVolume::CreateVolume()
{
 //if(dims.mag2()) {
 if(operator G4bool()) {
  if(!solid)  solid = new G4Box(name, dims.x(),
                          dims.y(),
                          dims.z());
  
  logic = new G4LogicalVolume(solid,      // its solid
                              mater,     // its material
                              name,     // its name
                              NULL,    // field
                              NULL,   // sensitive detector
                              NULL); // user limits
  
  physic = new G4PVPlacement(NULL,             // no rotation
                             pos,             // its position
                             logic,          // its logical volume
                             name,          // its name
            mother ? mother->logic : NULL, // its mother volume
                             false,       // no boolean operations
                             0);         // copy number
 }
}

void BoxVolume::CreateVolume(G4double& currentpos, const G4double& gap_after)
{
  SetPos(currentpos, gap_after);
  CreateVolume();
}


void BoxVolume::SetPos(G4double& currentpos, const G4double& gap_after)
{
  pos.set(currentpos + dims.x(), 0, 0);
  currentpos += 2*dims.x() + gap_after;

}


void BoxVolumes::Add(const BoxVolume& vol)
{
  push_back(vol);
  ++current;
}

BoxVolume BoxVolumes::DefaultChild()
{
  BoxVolume vol;
  vol.mother = mother;
  vol.attr = attr;
  vol.mater = mater;
  vol.name = name;
  return vol;
}

void BoxVolumes::CreateVolume()
{
  tFORs(std::vector<BoxVolume>::iterator, it, begin(), end()) {
    it->CreateVolume();
  }
}

G4Material* BoxVolumes::operator=(G4Material* stuff)
{
  tFORs(std::vector<BoxVolume>::iterator, it, begin(), end())
    it->SetMater(stuff);
  return stuff;
}


#define NAME_IF_THIS_EXIST(tst,obj) ((tst)&&(obj)?obj->GetName():"Not exist")
#define NAME_IF_EXIST(obj) NAME_IF_THIS_EXIST(obj,obj)
void BoxVolume::Print(G4bool private_) const
{
  if(!private_) G4cout << G4endl;
  G4cout << "  * * * * * * * * * * * * * * * * * * * * * * * * * * * *" << G4endl;
  G4cout << "  *  name    :  " << name << G4endl;
  G4cout << "  *  pos     :  " << pos/mm << " mm" << G4endl;
  G4cout << "  *  dims    :  " << dims/mm << " mm" << G4endl;
  G4cout << "  *  mater   :  " << NAME_IF_EXIST(mater) << G4endl;
  G4cout << "  *  physic  :  " << NAME_IF_EXIST(physic) << G4endl;
  G4cout << "  *  logic   :  " << NAME_IF_EXIST(logic) << G4endl;
  G4cout << "  *  mother  :  " << NAME_IF_THIS_EXIST(mother,mother->logic) << G4endl;
  if(!private_) G4cout << "  * * * * * * * * * * * * * * * * * * * * * * * * * * * *" << G4endl;
  if(!private_) G4cout << G4endl;
}

void BoxVolumes::Print() const
{
  G4cout << G4endl;
  G4cout << "  * * * * * * * * * * * * * * * * * * * * * * * * * * * *" << G4endl;
  G4cout << "  *  name    :  " << name << G4endl;
  G4cout << "  *  nlayers :  " << nlayers << G4endl;
  G4cout << "  *  size    :  " << size()  << G4endl;
  tFORs(std::vector<BoxVolume>::const_iterator, it, begin(), end()) {
    it->Print(true);
  }
  G4cout << "  * * * * * * * * * * * * * * * * * * * * * * * * * * * *" << G4endl;
  G4cout << G4endl;
}
