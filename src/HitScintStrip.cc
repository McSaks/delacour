#include "HitScintStrip.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

G4Allocator<HitScintStrip> hitAllocatorScintStrip;


HitScintStrip::HitScintStrip()
  : trackID(-1), edep(0)
{}

HitScintStrip::~HitScintStrip() {}

HitScintStrip::HitScintStrip(const HitScintStrip& right)
  : G4VHit()
{
  trackID   = right.trackID;
  stripNum  = right.stripNum;
  edep      = right.edep;
  pos       = right.pos;
}

const HitScintStrip& HitScintStrip::operator=(const HitScintStrip& right)
{
  trackID   = right.trackID;
  stripNum  = right.stripNum;
  edep      = right.edep;
  pos       = right.pos;
  return *this;
}

G4int HitScintStrip::operator==(const HitScintStrip& right) const
{
  return (this==&right) ? 1 : 0;
}

void HitScintStrip::Print()
{
  G4cout << "  track ID:       " << trackID << '\n';
   if(stripNum.y-kUnsensitive)
  G4cout << "  stripY #:       " << stripNum.y << '\n';
   if(stripNum.z-kUnsensitive)
  G4cout << "  stripZ #:       " << stripNum.z << '\n';
  G4cout << "  energy deposit: " << G4BestUnit(edep,"Energy") << '\n';
  G4cout << "  position:       " << G4BestUnit(pos,"Length") << G4endl;
}
