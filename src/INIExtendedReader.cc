#include "INIExtendedReader.hh"
#include <sstream>
#include <fstream>
#include "FOR.hh"
using namespace std;

//map<G4String, G4double> Geometry::unit;

void ErrorRedundant(const string& section, const string&  name, const string&  type)
{
  G4Exception(
    "INIExtenedReader::ReadU",
    ("Error in [" + section + "]." + name + " of type " + type).c_str(),
    FatalException,
    "Something redundant in the value.");
}
void ErrorArrayExtra(const string& section, const string&  name, const string&  type)
{
  G4Exception(
    "INIExtenedReader::ReadU",
    ("Error in [" + section + "]." + name + " of type " + type).c_str(),
    FatalException,
    "Extra values in the array.");
}
void ErrorArrayMissing(const string& section, const string&  name, const string&  type)
{
  G4Exception(
    "INIExtenedReader::ReadU",
    ("Error in [" + section + "]." + name + " of type " + type).c_str(),
    FatalException,
    "Missing values in the array.");
}

ValueBv::operator vector<bool>() const { return INIExtendedReader::UnValue(*this); }
ValueUv::operator vector<double>() const { return INIExtendedReader::UnValue(*this); }
ValueIv::operator vector<int>() const { return INIExtendedReader::UnValue_i(*this); }
ValueIv::operator vector<long>() const { return INIExtendedReader::UnValue_l(*this); }

INIExtendedReader::INIExtendedReader(const string& ifile)
  : INIReader(ifile), ok(true), kw_all("all"), kw_same("same")
{
  char c;
  ifstream infile(ifile.c_str());
  while( (infile.get(c), infile.unget(), c == '@') ) {
    infile.get(c);
    file_name.erase();
    while( (infile.get(c), c == ' ') && !infile.eof() ) { }
    infile.unget();
    while( (infile.get(c), infile.unget(), c != '\n') && !infile.eof() ) { file_name += c; infile.get(c); }
    //file_name = FRDirectory("in", file_name);
    infile.close();
    infile.open(file_name.c_str());
    if(!infile) {G4cerr << "Error: redirection target file ‘" << file_name << "’ cannot be found." << G4endl; ok = false; return; }
    else {G4cout << "redirection target file ‘" << file_name << "’ opened." << G4endl;}
  }
  infile.close();
  Parse();
}

INIExtendedReader::~INIExtendedReader()
{ }


ValueU INIExtendedReader::ReadU(const string& section, const string& name, const ValueU& dflt, double radlength)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return dflt;
  stringstream str(raw);
  double v; string u, rest;
  str >> v >> u >> rest;
  if(!rest.empty())
    ErrorRedundant(section, name, "denominate");
  //v = atof(s);
  return ValueU(v, u, radlength);
}

ValueSv INIExtendedReader::ReadSv(const string& section, const string& name, int n, const ValueS& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return ValueSv(n, dflt);
  stringstream str(raw);
  string s1, rest;
  // 0
  if(n == 0) {
    str >> s1 >> rest;
    if(!s1.empty() && rest != kw_all) ErrorRedundant(section, name, "array[string]");
    return ValueSv();
  }
  // 1
  ValueSv vec; vec.reserve(n);
  ValueS cur, prev;
  str >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[string]");
    str >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[string]");
    if(!str.eof()) ErrorArrayExtra(section, name, "array[string]");
    return vec;
  }
  // 2..n
  FOR(i, 2, n) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[string]");
    prev = cur;
    str >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = rest;
    vec.push_back(cur);
  }
  if(!str.eof()) ErrorArrayExtra(section, name, "array[string]");
  
  return vec;
}

ValueIv INIExtendedReader::ReadIv(const string& section, const string& name, int n, const ValueI& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return ValueIv(n, dflt);
  stringstream str(raw);
  string s1, rest;
  // 0
  if(n == 0) {
    str >> s1 >> rest;
    if(!s1.empty() && rest != kw_all) ErrorRedundant(section, name, "array[integer]");
    return ValueIv();
  }
  // 1
  ValueIv vec; vec.reserve(n);
  ValueI cur, prev;
  str >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[integer]");
    str >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[integer]");
    if(!str.eof()) ErrorArrayExtra(section, name, "array[integer]");
    return vec;
  }
  // 2..n
  FOR(i, 2, n) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[integer]");
    prev = cur;
    str >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atoi(rest.c_str());
    vec.push_back(cur);
  }
  if(!str.eof()) ErrorArrayExtra(section, name, "array[integer]");
  
  return vec;
}

ValueBv INIExtendedReader::ReadBv(const string& section, const string& name, int n, const ValueB& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return ValueBv(n, dflt);
  stringstream str(raw);
  string s1, rest;
  // 0
  if(n == 0) {
    str >> s1 >> rest;
    if(!s1.empty() && rest != kw_all) ErrorRedundant(section, name, "array[boolean]");
    return ValueBv();
  }
  // 1
  ValueBv vec; vec.reserve(n);
  string cur, prev;
  str >> cur;
  vec.push_back(ValueB(cur));
  if(n == 1) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[boolean]");
    str >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[boolean]");
    if(!str.eof()) ErrorArrayExtra(section, name, "array[boolean]");
    return vec;
  }
  // 2..n
  FOR(i, 2, n) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[boolean]");
    prev = cur;
    str >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = rest;
    vec.push_back(ValueB(cur));
  }
  if(!str.eof()) ErrorArrayExtra(section, name, "array[boolean]");
  
  return vec;
}

ValueDv INIExtendedReader::ReadDv(const string& section, const string& name, int n, const ValueD& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return ValueDv(n, dflt);
  stringstream str(raw);
  string s1, rest;
  // 0
  if(n == 0) {
    str >> s1 >> rest;
    if(!s1.empty() && rest != kw_all) ErrorRedundant(section, name, "array[real]");
    return ValueDv();
  }
  // 1
  ValueDv vec; vec.reserve(n);
  ValueD cur, prev;
  str >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[real]");
    str >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[real]");
    if(!str.eof()) ErrorArrayExtra(section, name, "array[real]");
    return vec;
  }
  // 2..n
  FOR(i, 2, n) {
    if(str.eof()) ErrorArrayMissing(section, name, "array[real]");
    prev = cur;
    str >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atof(rest.c_str());
    vec.push_back(cur);
  }
  if(!str.eof()) ErrorArrayExtra(section, name, "array[real]");
  
   return vec;
}

ValueUv INIExtendedReader::ReadUv(const string& section, const string& name, int n, const ValueU& dflt, double radlength)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return ValueUv(n, dflt);
  stringstream str(raw);
  string s1, s2;
  string rest;
  // 0
  if(n == 0) {
    str >> s1 >> s2 >> rest;
    if(!s1.empty() && !s2.empty() && rest != kw_all) ErrorRedundant(section, name, "array[denominate]");
    return ValueUv();
  }
  // 1, 2, 3
  ValueUv vec; vec.reserve(n);
  str >> s1;
  //if(str.eof()) ErrorArrayMissing();
  str >> s2;
  //if( s1 == kw_same  &&  s2 == kw_all ) { vec.assign(n, same); return vec; } // same all
  if( n == 1 ) { // 123 cm
    ValueU tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    if(!str.eof()) {
      str >> rest;
      if(rest != kw_all) ErrorArrayExtra(section, name, "array[denominate]"); // 123 cm all
    }
    if(!str.eof()) ErrorArrayExtra(section, name, "array[denominate]");
    return vec;
  }
  string s3; str >> s3;
  if( s3 == kw_all ) { // 123 cm all
    ValueU tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    return vec;
  }
  if( n == 2 ) { // 123 456 cm
    if(!str.eof()) ErrorArrayExtra(section, name, "array[denominate]");
    ValueU tmp1(s1, s3, radlength), tmp2(s2, s3, radlength);
    vec.push_back(tmp1);
    vec.push_back(tmp2);
    return vec;
  }
  // else( n >= 3 ) // 123 456 789 987 654 321 cm
  ValueDv vecD; vecD.reserve(n);
  ValueD cur, prev;
  // 1
  cur = atof(s1);
  vecD.push_back(cur); prev = cur;
  // 2
  if( s2 == kw_same ) cur = prev;
  else                cur = atof(s2);
  vecD.push_back(cur); prev = cur;
  // 3
  if( s3 == kw_same ) cur = prev;
  else                cur = atof(s3);
  vecD.push_back(cur); prev = cur;
  // 4..n
  FOR(i, 4, n) {
    str >> s1;
    if( s1 == kw_same ) cur = prev;
    else                cur = atof(s1);
    vecD.push_back(cur); prev = cur;
  }
  // unit
  if(str.eof()) ErrorArrayMissing(section, name, "array[denominate]");
  str >> s1;
  if(s1.empty()) ErrorArrayMissing(section, name, "array[denominate]");
  if(!str.eof()) ErrorArrayExtra(section, name, "array[denominate]");
  
  return AddUnit(vecD, s1, radlength);
}

ValueUv INIExtendedReader::AddUnit(const ValueDv& vecD, const string& u, double radlength)
{
  size_t n = vecD.size();
  ValueUv vec; vec.reserve(n);
  vFORc(ValueD, vecD, it) {
    vec.push_back(ValueU(*it, u, radlength));
  }
  return vec;
}

ValueUv INIExtendedReader::AddUnit(const ValueDv& vecD, const string& u, const vector<double>& radlength)
{
  size_t n = vecD.size(), i = 0;
  ValueUv vec; vec.reserve(n);
  vFORc(ValueD, vecD, it) {
    vec.push_back(ValueU(*it, u, radlength[i++]));
  }
  return vec;
}

vector<string> INIExtendedReader::UnValue(const ValueSv& val)
{
  vector<string> vec; string tmp;
  vFORc(ValueS, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<long> INIExtendedReader::UnValue_l(const ValueIv& val)
{
  vector<long> vec; long tmp;
  vFORc(ValueI, val, it)
    tmp = (long)*it, vec.push_back(tmp);
  return vec;
}

vector<int> INIExtendedReader::UnValue_i(const ValueIv& val)
{
  vector<int> vec; int tmp;
  vFORc(ValueI, val, it)
    tmp = (int)*it, vec.push_back(tmp);
  return vec;
}

vector<bool> INIExtendedReader::UnValue(const ValueBv& val)
{
  vector<bool> vec; bool tmp;
  vFORc(ValueB, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<double> INIExtendedReader::UnValue(const ValueDv& val)
{
  vector<double> vec;
  vFORc(ValueD, val, it)
    vec.push_back(G4double(*it));
  return vec;
}

vector<G4double> INIExtendedReader::UnValue(const ValueUv& val)
{
  vector<double> vec;
  vFORc(ValueU, val, it)
    vec.push_back(G4double(*it));
  return vec;
}

string INIExtendedReader::Get(const string& section, const string& name, const string& default_value, int depth)
{
  string result = INIReader::Get(section, name, default_value, depth);
  cout << "|";
  FOR (i, 2, depth) cout << " |     ";
  if (depth > 0) cout << " '-----";
  cout << " Getting [" + section + "]." + name << " = " << result << endl;
  return result;
}

/*string INIExtendedReader::ShowItems(int n)
{
  unless (n) return string();
  long pos = infile.tellg();
  string o;
  o = ReadItem();
  FORs(i, 1, n)
    o += ' ' + ReadItem();
  infile.seekg(pos);
  return o;
}*/
