data=$work10/Myrtle

Myrtle                        \
  --trigger none               \
  --geometry col                \
  --macro vert                   \
  --nevents 3000                  \
  {                                \
    /gun/particle proton ,          \
    /gun/energy 250 GeV              \
  }                                   \
  --out $data/out/rejection/vert/p+250 \
  1> $data/log/rejection/vert/p+250     \
  2> $data/log/rejection/vert/p+250-err  \
