data=$work10/Myrtle

Myrtle                        \
  --trigger none               \
  --geometry col                \
  --macro vert                   \
  --nevents 3000                  \
  {                                \
    /gun/particle e- ,              \
    /gun/energy 100 GeV              \
  }                                   \
  --out $data/out/rejection/vert/e-100 \
  1> $data/log/rejection/vert/e-100     \
  2> $data/log/rejection/vert/e-100-err  \
