data=$work10/Myrtle

Myrtle                        \
  --trigger none               \
  --geometry col                \
  --macro vert                   \
  --nevents 3000                  \
  {                                \
    /gun/particle proton ,          \
    /gun/energy 150 GeV              \
  }                                   \
  --out $data/out/rejection/vert/p+150 \
  1> $data/log/rejection/vert/p+150     \
  2> $data/log/rejection/vert/p+150-err  \
