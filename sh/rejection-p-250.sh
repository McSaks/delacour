data=$work10/Myrtle

Myrtle                   \
  --trigger none          \
  --geometry cubital       \
  --macro side              \
  --nevents 2000             \
  {                           \
    /gun/particle proton ,     \
    /gun/energy 250 GeV         \
  }                              \
  --out $data/out/rejection/cubital/p+250 \
  1> $data/log/rejection/cubital/p+250     \
  2> $data/log/rejection/cubital/p+250-err  \
