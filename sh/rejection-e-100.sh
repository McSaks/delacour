data=$work10/Myrtle

Myrtle                   \
  --trigger none          \
  --geometry cubital       \
  --macro side              \
  --nevents 2000             \
  {                           \
    /gun/particle e- ,         \
    /gun/energy 100 GeV         \
  }                              \
  --out $data/out/rejection/cubital/e-100 \
  1> $data/log/rejection/cubital/e-100     \
  2> $data/log/rejection/cubital/e-100-err  \
